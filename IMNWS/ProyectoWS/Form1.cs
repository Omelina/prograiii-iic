﻿using System;
using ProyectoWS.IMN_WS;
using System.Windows.Forms;
using DemoWebService;

namespace ProyectoWS
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            boxCiudades.Text = "Seleccione";
            for (int i = 1; i <=16; i++)
            {
                CargarCiudades(i);
            }

        }

        private void CargarCiudades(int id)
        {
            WSMeteorologicoClient ws = new WSMeteorologicoClient();
            PRONOSTICO_PORCIUDADES reg = ws.pronosticoPorCiudadxID(id).ParseXML<PRONOSTICO_PORCIUDADES>();
            boxCiudades.Items.AddRange(reg.CIUDADES);
        }

        private void boxCiudades_SelectedIndexChanged(object sender, EventArgs e)
        {
            WSMeteorologicoClient ws = new WSMeteorologicoClient();
            if (boxCiudades.SelectedIndex >= 0)
            {
                int id = (boxCiudades.SelectedItem as PRONOSTICO_REGIONALREGIONCIUDAD)?.id?? -1;
                if (id != -1)
                {
                    PRONOSTICO_PORCIUDADES reg = ws.pronosticoPorCiudadxID(id).ParseXML<PRONOSTICO_PORCIUDADES>();
                    EFEMERIDES fl = ws.efemerides(new efemerides()).ParseXML<EFEMERIDES>();
                    if (reg.CIUDADES[0].ESTADOMANANA != null)
                    {
                        pbxImagenM.ImageLocation = String.Format("https://www.imn.ac.cr{0}", reg.CIUDADES[0].ESTADOMANANA.imgPath);
                        lblComentarioM.Text = reg.CIUDADES[0].COMENTARIOMANANA;
                        lblTituloM.Text = reg.CIUDADES[0].ESTADOMANANA.Value;
                        pnlMañanaR.Show();
                    }

                    if (reg.CIUDADES[0].ESTADOTARDE != null)
                    {
                        pbxImagenT.ImageLocation = String.Format("https://www.imn.ac.cr{0}", reg.CIUDADES[0].ESTADOTARDE.imgPath);
                        lblComentarioT.Text = reg.CIUDADES[0].COMENTARIOTARDE;
                        lblTituloT.Text = reg.CIUDADES[0].ESTADOTARDE.Value;
                        pnlTardeR.Show();
                    }
                    if (reg.CIUDADES[0].ESTADONOCHE != null)
                    {
                        pbxImagenN.ImageLocation = String.Format("https://www.imn.ac.cr{0}", reg.CIUDADES[0].ESTADONOCHE.imgPath);
                        lblComentarioN.Text = reg.CIUDADES[0].COMENTARIONOCHE;
                        lblTituloN.Text = reg.CIUDADES[0].ESTADONOCHE.Value;
                        pnlNocheR.Show();
                    }
                    if (reg.CIUDADES[0].ESTADOMADRUGADA != null)
                    {
                        pbxImagenMa.ImageLocation = String.Format("https://www.imn.ac.cr{0}", reg.CIUDADES[0].ESTADOMADRUGADA.imgPath);
                        lblComentarioMa.Text = reg.CIUDADES[0].COMENTARIOMADRUGADA;
                        lblTituloMa.Text = reg.CIUDADES[0].ESTADOMADRUGADA.Value;
                        pnlMadrugada.Show();
                    }
                    if (reg.CIUDADES[0].TEMPMAX != 0)
                    {
                        txtTMax.Text = reg.CIUDADES[0].TEMPMAX.ToString();
                    }
                    if (reg.CIUDADES[0].TEMPMIN != 0)
                    {
                        txtTMin.Text = reg.CIUDADES[0].TEMPMIN.ToString();
                    }
                    if (fl.FASELUNAR != null)
                    {
                        lblFaseLunar.Text = fl.FASELUNAR.Value.ToString();
                    }
                    if (fl.EFEMERIDE_SOL != null)
                    {
                        lblSaleSol.Text = fl.EFEMERIDE_SOL.SALE;
                        lblPoneSol.Text = fl.EFEMERIDE_SOL.SEPONE;
                    }if (fl.EFEMERIDE_LUNA != null)
                    {
                        lblSaleLuna.Text = fl.EFEMERIDE_LUNA.SALE;
                        lblPoneLuna.Text = fl.EFEMERIDE_LUNA.SEPONE;
                    }

                }
            }
        }
    }
}
