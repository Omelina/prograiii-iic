﻿namespace ProyectoWS
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.pnlMañanaR = new System.Windows.Forms.Panel();
            this.lblComentarioM = new System.Windows.Forms.TextBox();
            this.lblTituloM = new System.Windows.Forms.Label();
            this.pbxImagenM = new System.Windows.Forms.PictureBox();
            this.pnlTardeR = new System.Windows.Forms.Panel();
            this.lblComentarioT = new System.Windows.Forms.TextBox();
            this.lblTituloT = new System.Windows.Forms.Label();
            this.pbxImagenT = new System.Windows.Forms.PictureBox();
            this.pnlNocheR = new System.Windows.Forms.Panel();
            this.lblComentarioN = new System.Windows.Forms.TextBox();
            this.lblTituloN = new System.Windows.Forms.Label();
            this.pbxImagenN = new System.Windows.Forms.PictureBox();
            this.boxCiudades = new System.Windows.Forms.ComboBox();
            this.pnlMadrugada = new System.Windows.Forms.Panel();
            this.lblComentarioMa = new System.Windows.Forms.TextBox();
            this.lblTituloMa = new System.Windows.Forms.Label();
            this.pbxImagenMa = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTMax = new System.Windows.Forms.TextBox();
            this.txtTMin = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pbxSol = new System.Windows.Forms.PictureBox();
            this.pbxLuna = new System.Windows.Forms.PictureBox();
            this.pbxFaseLunar = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblFaseLunar = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblSaleSol = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblPoneSol = new System.Windows.Forms.Label();
            this.lblPoneLuna = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblSaleLuna = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox4.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.pnlMañanaR.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenM)).BeginInit();
            this.pnlTardeR.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenT)).BeginInit();
            this.pnlNocheR.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenN)).BeginInit();
            this.pnlMadrugada.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenMa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxLuna)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxFaseLunar)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lblPoneLuna);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.lblSaleLuna);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.lblPoneSol);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.lblSaleSol);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.lblFaseLunar);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.pbxFaseLunar);
            this.groupBox4.Controls.Add(this.pbxLuna);
            this.groupBox4.Controls.Add(this.pbxSol);
            this.groupBox4.Controls.Add(this.txtTMin);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.txtTMax);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.pnlMadrugada);
            this.groupBox4.Controls.Add(this.boxCiudades);
            this.groupBox4.Controls.Add(this.flowLayoutPanel1);
            this.groupBox4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(12, 11);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Size = new System.Drawing.Size(687, 513);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Pronóstico";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.pnlMañanaR);
            this.flowLayoutPanel1.Controls.Add(this.pnlTardeR);
            this.flowLayoutPanel1.Controls.Add(this.pnlNocheR);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(21, 96);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(349, 394);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // pnlMañanaR
            // 
            this.pnlMañanaR.Controls.Add(this.lblComentarioM);
            this.pnlMañanaR.Controls.Add(this.lblTituloM);
            this.pnlMañanaR.Controls.Add(this.pbxImagenM);
            this.pnlMañanaR.Location = new System.Drawing.Point(3, 2);
            this.pnlMañanaR.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlMañanaR.Name = "pnlMañanaR";
            this.pnlMañanaR.Size = new System.Drawing.Size(348, 95);
            this.pnlMañanaR.TabIndex = 0;
            // 
            // lblComentarioM
            // 
            this.lblComentarioM.BackColor = System.Drawing.SystemColors.Control;
            this.lblComentarioM.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblComentarioM.Enabled = false;
            this.lblComentarioM.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComentarioM.Location = new System.Drawing.Point(101, 39);
            this.lblComentarioM.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lblComentarioM.Multiline = true;
            this.lblComentarioM.Name = "lblComentarioM";
            this.lblComentarioM.ReadOnly = true;
            this.lblComentarioM.Size = new System.Drawing.Size(237, 47);
            this.lblComentarioM.TabIndex = 2;
            this.lblComentarioM.Text = "Comentarios";
            // 
            // lblTituloM
            // 
            this.lblTituloM.AutoSize = true;
            this.lblTituloM.Location = new System.Drawing.Point(95, 14);
            this.lblTituloM.Name = "lblTituloM";
            this.lblTituloM.Size = new System.Drawing.Size(73, 25);
            this.lblTituloM.TabIndex = 1;
            this.lblTituloM.Text = "Estado";
            // 
            // pbxImagenM
            // 
            this.pbxImagenM.Location = new System.Drawing.Point(3, 4);
            this.pbxImagenM.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pbxImagenM.Name = "pbxImagenM";
            this.pbxImagenM.Size = new System.Drawing.Size(88, 85);
            this.pbxImagenM.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxImagenM.TabIndex = 0;
            this.pbxImagenM.TabStop = false;
            // 
            // pnlTardeR
            // 
            this.pnlTardeR.Controls.Add(this.lblComentarioT);
            this.pnlTardeR.Controls.Add(this.lblTituloT);
            this.pnlTardeR.Controls.Add(this.pbxImagenT);
            this.pnlTardeR.Location = new System.Drawing.Point(3, 101);
            this.pnlTardeR.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlTardeR.Name = "pnlTardeR";
            this.pnlTardeR.Size = new System.Drawing.Size(348, 95);
            this.pnlTardeR.TabIndex = 1;
            // 
            // lblComentarioT
            // 
            this.lblComentarioT.BackColor = System.Drawing.SystemColors.Control;
            this.lblComentarioT.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblComentarioT.Enabled = false;
            this.lblComentarioT.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComentarioT.Location = new System.Drawing.Point(101, 42);
            this.lblComentarioT.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lblComentarioT.Multiline = true;
            this.lblComentarioT.Name = "lblComentarioT";
            this.lblComentarioT.ReadOnly = true;
            this.lblComentarioT.Size = new System.Drawing.Size(237, 47);
            this.lblComentarioT.TabIndex = 2;
            this.lblComentarioT.Text = "Comentarios";
            // 
            // lblTituloT
            // 
            this.lblTituloT.AutoSize = true;
            this.lblTituloT.Location = new System.Drawing.Point(99, 10);
            this.lblTituloT.Name = "lblTituloT";
            this.lblTituloT.Size = new System.Drawing.Size(73, 25);
            this.lblTituloT.TabIndex = 1;
            this.lblTituloT.Text = "Estado";
            // 
            // pbxImagenT
            // 
            this.pbxImagenT.Location = new System.Drawing.Point(3, 5);
            this.pbxImagenT.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pbxImagenT.Name = "pbxImagenT";
            this.pbxImagenT.Size = new System.Drawing.Size(88, 85);
            this.pbxImagenT.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxImagenT.TabIndex = 0;
            this.pbxImagenT.TabStop = false;
            // 
            // pnlNocheR
            // 
            this.pnlNocheR.Controls.Add(this.lblComentarioN);
            this.pnlNocheR.Controls.Add(this.lblTituloN);
            this.pnlNocheR.Controls.Add(this.pbxImagenN);
            this.pnlNocheR.Location = new System.Drawing.Point(3, 200);
            this.pnlNocheR.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlNocheR.Name = "pnlNocheR";
            this.pnlNocheR.Size = new System.Drawing.Size(348, 95);
            this.pnlNocheR.TabIndex = 2;
            // 
            // lblComentarioN
            // 
            this.lblComentarioN.BackColor = System.Drawing.SystemColors.Control;
            this.lblComentarioN.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblComentarioN.Enabled = false;
            this.lblComentarioN.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComentarioN.Location = new System.Drawing.Point(104, 39);
            this.lblComentarioN.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lblComentarioN.Multiline = true;
            this.lblComentarioN.Name = "lblComentarioN";
            this.lblComentarioN.ReadOnly = true;
            this.lblComentarioN.Size = new System.Drawing.Size(232, 47);
            this.lblComentarioN.TabIndex = 2;
            this.lblComentarioN.Text = "Comentarios";
            // 
            // lblTituloN
            // 
            this.lblTituloN.AutoSize = true;
            this.lblTituloN.Location = new System.Drawing.Point(99, 10);
            this.lblTituloN.Name = "lblTituloN";
            this.lblTituloN.Size = new System.Drawing.Size(73, 25);
            this.lblTituloN.TabIndex = 1;
            this.lblTituloN.Text = "Estado";
            // 
            // pbxImagenN
            // 
            this.pbxImagenN.Location = new System.Drawing.Point(3, 2);
            this.pbxImagenN.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pbxImagenN.Name = "pbxImagenN";
            this.pbxImagenN.Size = new System.Drawing.Size(88, 85);
            this.pbxImagenN.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxImagenN.TabIndex = 0;
            this.pbxImagenN.TabStop = false;
            // 
            // boxCiudades
            // 
            this.boxCiudades.DisplayMember = "nombre";
            this.boxCiudades.FormattingEnabled = true;
            this.boxCiudades.Location = new System.Drawing.Point(21, 43);
            this.boxCiudades.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.boxCiudades.Name = "boxCiudades";
            this.boxCiudades.Size = new System.Drawing.Size(351, 33);
            this.boxCiudades.TabIndex = 2;
            this.boxCiudades.ValueMember = "idRegion";
            this.boxCiudades.SelectedIndexChanged += new System.EventHandler(this.boxCiudades_SelectedIndexChanged);
            // 
            // pnlMadrugada
            // 
            this.pnlMadrugada.Controls.Add(this.lblComentarioMa);
            this.pnlMadrugada.Controls.Add(this.lblTituloMa);
            this.pnlMadrugada.Controls.Add(this.pbxImagenMa);
            this.pnlMadrugada.Location = new System.Drawing.Point(21, 395);
            this.pnlMadrugada.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlMadrugada.Name = "pnlMadrugada";
            this.pnlMadrugada.Size = new System.Drawing.Size(349, 95);
            this.pnlMadrugada.TabIndex = 3;
            // 
            // lblComentarioMa
            // 
            this.lblComentarioMa.BackColor = System.Drawing.SystemColors.Control;
            this.lblComentarioMa.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblComentarioMa.Enabled = false;
            this.lblComentarioMa.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComentarioMa.Location = new System.Drawing.Point(104, 39);
            this.lblComentarioMa.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lblComentarioMa.Multiline = true;
            this.lblComentarioMa.Name = "lblComentarioMa";
            this.lblComentarioMa.ReadOnly = true;
            this.lblComentarioMa.Size = new System.Drawing.Size(232, 47);
            this.lblComentarioMa.TabIndex = 2;
            this.lblComentarioMa.Text = "Comentarios";
            // 
            // lblTituloMa
            // 
            this.lblTituloMa.AutoSize = true;
            this.lblTituloMa.Location = new System.Drawing.Point(99, 10);
            this.lblTituloMa.Name = "lblTituloMa";
            this.lblTituloMa.Size = new System.Drawing.Size(73, 25);
            this.lblTituloMa.TabIndex = 1;
            this.lblTituloMa.Text = "Estado";
            // 
            // pbxImagenMa
            // 
            this.pbxImagenMa.Location = new System.Drawing.Point(3, 2);
            this.pbxImagenMa.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pbxImagenMa.Name = "pbxImagenMa";
            this.pbxImagenMa.Size = new System.Drawing.Size(88, 85);
            this.pbxImagenMa.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxImagenMa.TabIndex = 0;
            this.pbxImagenMa.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(444, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 25);
            this.label1.TabIndex = 4;
            this.label1.Text = "T. Max. °C";
            // 
            // txtTMax
            // 
            this.txtTMax.BackColor = System.Drawing.SystemColors.Control;
            this.txtTMax.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTMax.Enabled = false;
            this.txtTMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTMax.Location = new System.Drawing.Point(449, 72);
            this.txtTMax.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTMax.Multiline = true;
            this.txtTMax.Name = "txtTMax";
            this.txtTMax.ReadOnly = true;
            this.txtTMax.Size = new System.Drawing.Size(101, 42);
            this.txtTMax.TabIndex = 3;
            this.txtTMax.Text = "38";
            this.txtTMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtTMin
            // 
            this.txtTMin.BackColor = System.Drawing.SystemColors.Control;
            this.txtTMin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTMin.Enabled = false;
            this.txtTMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTMin.Location = new System.Drawing.Point(570, 72);
            this.txtTMin.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTMin.Multiline = true;
            this.txtTMin.Name = "txtTMin";
            this.txtTMin.ReadOnly = true;
            this.txtTMin.Size = new System.Drawing.Size(101, 42);
            this.txtTMin.TabIndex = 5;
            this.txtTMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(565, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 25);
            this.label2.TabIndex = 6;
            this.label2.Text = "T. Min. °C";
            // 
            // pbxSol
            // 
            this.pbxSol.Image = ((System.Drawing.Image)(resources.GetObject("pbxSol.Image")));
            this.pbxSol.Location = new System.Drawing.Point(449, 306);
            this.pbxSol.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pbxSol.Name = "pbxSol";
            this.pbxSol.Size = new System.Drawing.Size(88, 85);
            this.pbxSol.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxSol.TabIndex = 3;
            this.pbxSol.TabStop = false;
            // 
            // pbxLuna
            // 
            this.pbxLuna.Image = ((System.Drawing.Image)(resources.GetObject("pbxLuna.Image")));
            this.pbxLuna.Location = new System.Drawing.Point(570, 306);
            this.pbxLuna.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pbxLuna.Name = "pbxLuna";
            this.pbxLuna.Size = new System.Drawing.Size(88, 85);
            this.pbxLuna.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxLuna.TabIndex = 7;
            this.pbxLuna.TabStop = false;
            // 
            // pbxFaseLunar
            // 
            this.pbxFaseLunar.Image = ((System.Drawing.Image)(resources.GetObject("pbxFaseLunar.Image")));
            this.pbxFaseLunar.Location = new System.Drawing.Point(509, 147);
            this.pbxFaseLunar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pbxFaseLunar.Name = "pbxFaseLunar";
            this.pbxFaseLunar.Size = new System.Drawing.Size(88, 85);
            this.pbxFaseLunar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxFaseLunar.TabIndex = 8;
            this.pbxFaseLunar.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(439, 249);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "Fase Lunar:";
            // 
            // lblFaseLunar
            // 
            this.lblFaseLunar.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFaseLunar.Location = new System.Drawing.Point(529, 249);
            this.lblFaseLunar.Name = "lblFaseLunar";
            this.lblFaseLunar.Size = new System.Drawing.Size(129, 43);
            this.lblFaseLunar.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(446, 397);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 17);
            this.label5.TabIndex = 11;
            this.label5.Text = "Sale:";
            // 
            // lblSaleSol
            // 
            this.lblSaleSol.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSaleSol.Location = new System.Drawing.Point(446, 423);
            this.lblSaleSol.Name = "lblSaleSol";
            this.lblSaleSol.Size = new System.Drawing.Size(91, 17);
            this.lblSaleSol.TabIndex = 12;
            this.lblSaleSol.Text = "10:30 am";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(446, 450);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 17);
            this.label7.TabIndex = 13;
            this.label7.Text = "Se pone:";
            // 
            // lblPoneSol
            // 
            this.lblPoneSol.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPoneSol.Location = new System.Drawing.Point(446, 473);
            this.lblPoneSol.Name = "lblPoneSol";
            this.lblPoneSol.Size = new System.Drawing.Size(91, 17);
            this.lblPoneSol.TabIndex = 14;
            this.lblPoneSol.Text = "10:30 am";
            // 
            // lblPoneLuna
            // 
            this.lblPoneLuna.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPoneLuna.Location = new System.Drawing.Point(567, 473);
            this.lblPoneLuna.Name = "lblPoneLuna";
            this.lblPoneLuna.Size = new System.Drawing.Size(91, 17);
            this.lblPoneLuna.TabIndex = 18;
            this.lblPoneLuna.Text = "10:30 am";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(567, 450);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 17);
            this.label10.TabIndex = 17;
            this.label10.Text = "Se pone:";
            // 
            // lblSaleLuna
            // 
            this.lblSaleLuna.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSaleLuna.Location = new System.Drawing.Point(567, 423);
            this.lblSaleLuna.Name = "lblSaleLuna";
            this.lblSaleLuna.Size = new System.Drawing.Size(91, 17);
            this.lblSaleLuna.TabIndex = 16;
            this.lblSaleLuna.Text = "10:30 am";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(567, 397);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 17);
            this.label12.TabIndex = 15;
            this.label12.Text = "Sale:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(711, 535);
            this.Controls.Add(this.groupBox4);
            this.Name = "Form1";
            this.Text = "Pronóstico del Tiempo por Región";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.pnlMañanaR.ResumeLayout(false);
            this.pnlMañanaR.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenM)).EndInit();
            this.pnlTardeR.ResumeLayout(false);
            this.pnlTardeR.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenT)).EndInit();
            this.pnlNocheR.ResumeLayout(false);
            this.pnlNocheR.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenN)).EndInit();
            this.pnlMadrugada.ResumeLayout(false);
            this.pnlMadrugada.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenMa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxLuna)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxFaseLunar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel pnlMañanaR;
        private System.Windows.Forms.TextBox lblComentarioM;
        private System.Windows.Forms.Label lblTituloM;
        private System.Windows.Forms.PictureBox pbxImagenM;
        private System.Windows.Forms.Panel pnlTardeR;
        private System.Windows.Forms.TextBox lblComentarioT;
        private System.Windows.Forms.Label lblTituloT;
        private System.Windows.Forms.PictureBox pbxImagenT;
        private System.Windows.Forms.Panel pnlNocheR;
        private System.Windows.Forms.TextBox lblComentarioN;
        private System.Windows.Forms.Label lblTituloN;
        private System.Windows.Forms.PictureBox pbxImagenN;
        private System.Windows.Forms.ComboBox boxCiudades;
        private System.Windows.Forms.Panel pnlMadrugada;
        private System.Windows.Forms.TextBox lblComentarioMa;
        private System.Windows.Forms.Label lblTituloMa;
        private System.Windows.Forms.PictureBox pbxImagenMa;
        private System.Windows.Forms.Label lblPoneLuna;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblSaleLuna;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblPoneSol;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblSaleSol;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblFaseLunar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pbxFaseLunar;
        private System.Windows.Forms.PictureBox pbxLuna;
        private System.Windows.Forms.PictureBox pbxSol;
        private System.Windows.Forms.TextBox txtTMin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTMax;
        private System.Windows.Forms.Label label1;
    }
}

