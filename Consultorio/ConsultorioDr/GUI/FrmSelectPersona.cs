﻿using BO;
using DAO;
using Daramee.TaskDialogSharp;
using Entities;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsultorioDr.GUI
{
    public partial class FrmSelectPersona : Form
    {
        public List<Persona> listap { get; set; }
        private Persona p  = new Persona();

        public FrmSelectPersona()
        {
            InitializeComponent();
            list.DataSource = listap;
        }

        public Persona GetPersona()
        {
            return p;
        }

        private void btnSeleccionar_Click(object sender, EventArgs e)
        {
            p = (Persona)list.SelectedItem;
            if (p != null)
            {
                Close();
            }

        }

        

        private void btnSalir_Click(object sender, EventArgs e)
        {
            p = null;
            Close();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            String filtro = txtBuscar.Text.Trim();
            if (filtro.Equals(""))
            {
                DialogResult con = MessageBox.Show("¿Está seguro que desea cargar todos los usuarios?",
                    "Buscar usuarios...", MessageBoxButtons.YesNo);
                if (con == DialogResult.No)
                {
                    return;
                }
            }
            cargarDatos(filtro);

        }

        private void txtBuscar_KeyUp(object sender, KeyEventArgs e)
        {
            String filtro = txtBuscar.Text.Trim();
            if (filtro.Length >= 3)
            {
                cargarDatos(filtro);
            }
            else
            {
                list.Items.Clear();
            }

        }

        private void txtBuscar_Click(object sender, EventArgs e)
        {
            txtBuscar.SelectionStart = 0;
        }

        private void cargarDatos(String filtro)
        {
            list.Items.Clear();
            PersonaBO pbo = new PersonaBO();
            foreach (Persona p in pbo.buscar(filtro))
            {
                list.Items.Add(p);
            }
        }
    }
}
