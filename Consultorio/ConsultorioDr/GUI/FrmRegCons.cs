﻿using BO;
using DAO;
using Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsultorioDr.GUI
{
    public partial class FrmRegCons : Form
    {
        Consulta consulta;
        ConsultaBO cbo = new ConsultaBO();
        ConsultaDAO cdao = new ConsultaDAO();

        public FrmRegCons()
        {
            this.CenterToParent();
            InitializeComponent();
            if (this.consulta == null)
            {
                this.consulta = new Consulta();
            }
            else
            {
                cargardatos();
            }
        }

        private void cargardatos()
        {
            txtCita.Text = consulta.numCita.ToString();
            txtPersona.Text = consulta.persona.nombre;
            txtPrecio.Text = consulta.precio.ToString();
            txtPade.Text = consulta.padecimiento;
            txtNotas.Text = consulta.notas;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            FrmSelectPersona frm = new FrmSelectPersona();
            Hide();
            frm.ShowDialog();
            Show();
        }
    }
}
