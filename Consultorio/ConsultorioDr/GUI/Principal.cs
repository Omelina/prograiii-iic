﻿using Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsultorioDr.GUI
{
    public partial class Principal : Form
    {
        public Principal()
        {
            this.CenterToScreen();
            InitializeComponent();
        }

        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Principal());
        }

        private void btnRegPersona_Click(object sender, EventArgs e)
        {
            FrmRegPer frm = new FrmRegPer(null);
            Hide();
            frm.ShowDialog();
            Show();
        }

        private void btnRegistrarCita_Click(object sender, EventArgs e)
        {
            FrmRegCons frm = new FrmRegCons();
            Hide();
            frm.ShowDialog();
            Show();
        }
    }
}
