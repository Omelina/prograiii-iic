﻿using BO;
using DAO;
using Entities;
using Microsoft.CSharp.RuntimeBinder;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsultorioDr.GUI
{
    public partial class FrmRegPer : Form
    {
        Persona persona;
        PersonaBO pbo = new PersonaBO();
        PersonaDAO pdao = new PersonaDAO();

        public FrmRegPer(Persona persona)
        {
            this.CenterToParent();
            InitializeComponent();
            if (this.persona == null)
            {
                this.persona = new Persona();
            }
            else
            {
                cargardatos();
            }
        }

        private void cargardatos()
        {
            txtCedula.Text = persona.cedula;
            txtNombre.Text = persona.nombre;
            txtEdad.Text =  persona.edad.ToString();
            txtTelefono.Text = persona.telefono.ToString();
            txtDirección.Text = persona.direccion;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Persona p = new Persona();
            {
                p.cedula = txtCedula.Text.Trim();
                p.nombre = txtNombre.Text.Trim();
                p.edad = Int32.Parse(txtEdad.Text.Trim());
                p.telefono = Int32.Parse(txtTelefono.Text.Trim());
                p.direccion = txtDirección.Text.Trim();
            };
            pbo.Registrar(p);
            Close();

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
