﻿using System;
using System.Collections.Generic;
using System.Text;
using Entities;
using Npgsql;

namespace DAO
{
    public class ConsultaDAO
    {
        static NpgsqlConnection con;
        //static NpgsqlCommand cmd;

        //public void InsertarConsulta(Consulta consulta)
        //{
        //    ClsConexion.Conexion();
        //    con.Open();
        //    cmd = new NpgsqlCommand("INSERT INTO cs.consulta(precio, id_persona, padecimiento, notas)" +
        //        " VALUES('" + consulta.precio + "', '"+ consulta.persona.id + "', '" + consulta.padecimiento
        //        + "', '" + consulta.notas + "')", con);
        //    cmd.ExecuteNonQuery();
        //    con.Close();
        //}

        public void InsertarConsulta(Consulta consulta)
        {
            string sql = "INSERT INTO cs.consulta(id, precio, num_cita, " +
                " padecimiento, notas, id_persona) VALUES (@pre, @num, @pad, @not, @idp)";
            con = ClsConexion.Conexion();
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@pre", consulta.precio);
                cmd.Parameters.AddWithValue("@num", consulta.numCita);
                cmd.Parameters.AddWithValue("@pad", consulta.padecimiento);
                cmd.Parameters.AddWithValue("@not", consulta.notas);
                cmd.Parameters.AddWithValue("@idp", consulta.persona.id);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public void ModificarConsulta(Consulta consulta)
        {
            string sql = "UPDATE cs.consulta SET precio = @pre, " +
                " num_cita = @num, padecimiento =@pad, notas =@not, id_persona = @idp,  WHERE id= @id ";
            con = ClsConexion.Conexion();
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@pre", consulta.precio);
                cmd.Parameters.AddWithValue("@num", consulta.numCita);
                cmd.Parameters.AddWithValue("@pad", consulta.padecimiento);
                cmd.Parameters.AddWithValue("@not", consulta.notas);
                cmd.Parameters.AddWithValue("@idp", consulta.persona.id);
                cmd.Parameters.AddWithValue("@id", consulta.idCita);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        //public void ModificarPersona( int id,double precio, Consulta consulta, int padecimiento, string notas)
        //{
        //    ClsConexion.Conexion();
        //    con.Open();
        //    NpgsqlCommand cmd = new NpgsqlCommand("UPDATE cs.consulta SET precio = '"+precio+"', '" +
        //        " id_persona = '"+consulta.persona.id+"', padecimiento ='"+padecimiento+"', " +
        //        " notas ='"+notas+"' WHERE id= '"+id+"'", con);
        //    cmd.ExecuteNonQuery();
        //    con.Close();
        //}

        private Consulta Cargar(NpgsqlDataReader reader)
        {
            Consulta con = new Consulta
            {
                idCita = reader.GetInt32(0),
                precio = reader.GetDouble(1),
                //new PersonaDAO().cargarPersona(con.persona.id),
                padecimiento = reader.GetString(2),
                notas = reader.GetString(3),
                numCita = reader.GetInt32(4)
            };
            return con;
        }


    }
}
