﻿using System;
using System.Collections.Generic;
using System.Text;
using Entities;
using Npgsql;

namespace DAO
{
    public class PersonaDAO
    {

        static NpgsqlConnection con;
        static NpgsqlCommand cmd;

        //public void InsertarPersona(Persona persona)
        //{
        //    ClsConexion.Conexion();
        //    con.Open();
        //    cmd = new NpgsqlCommand("INSERT INTO cs.persona (cedula, nombre, edad, telefono, direccion) VALUES ('" + persona.cedula + "', '"
        //        + persona.nombre + "', '" + persona.edad + "', '" + persona.telefono + "', '" + persona.direccion + "')", con);
        //    cmd.ExecuteNonQuery();
        //    con.Close();
        //}

        public void InsertarPersona(Persona persona)
        {
            string sql = "INSERT INTO cs.persona(cedula, nombre, " +
                " edad, telefono, direccion) VALUES(@ced, @nom, @edad, @tel, @dir)";
            con = ClsConexion.Conexion();
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@ced", persona.cedula);
                cmd.Parameters.AddWithValue("@nom", persona.nombre);
                cmd.Parameters.AddWithValue("@edad", persona.edad);
                cmd.Parameters.AddWithValue("@tel", persona.telefono);
                cmd.Parameters.AddWithValue("@dir", persona.direccion);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }


        //public void ModificarPersona(Persona persona)
        //{
        //    ClsConexion.Conexion();
        //    con.Open();
        //    NpgsqlCommand cmd = new NpgsqlCommand("UPDATE cs.persona SET nombre = '" + persona.nombre + "', edad = '"
        //        + persona.edad + "', telefono = '" + persona.telefono + "', telefono = '" + persona.direccion+"'" +
        //        " WHERE id = '" + persona.id + "'", con);
        //    cmd.ExecuteNonQuery();
        //    con.Close();
        //}

        public void ModificarPersona(Persona persona)
        {
            string sql = "UPDATE cs.persona SET cedula = @ced, " +
                " nombre = @nom, edad =@edad, telefono =@tel, direccion =@dir WHERE id = @id ";
            con = ClsConexion.Conexion();
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@ced", persona.cedula);
                cmd.Parameters.AddWithValue("@nom", persona.nombre);
                cmd.Parameters.AddWithValue("@edad", persona.edad);
                cmd.Parameters.AddWithValue("@tel", persona.telefono);
                cmd.Parameters.AddWithValue("@dir", persona.direccion);
                cmd.Parameters.AddWithValue("@id", persona.id);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public List<Persona> cargarPersona(Persona per)
        {
            con = ClsConexion.Conexion();
            List<Persona> personas = new List<Persona>();
            String sql = "SELECT id, cedula, nombre, edad, telefono, direccion FROM cs.persona";
            
            con.Open();
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
            cmd.Parameters.AddWithValue("@id", per.id);

            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                Persona temp = Cargar(reader);
                per.cedula = temp.cedula;
                per.nombre = temp.nombre;
                per.edad = temp.edad;
                per.id = temp.id;
                personas.Add(temp);
                return personas;
            }
            return null;

        }

        public List<Persona> seleccionar(string val)
        {
            con = ClsConexion.Conexion();
            List<Persona> personas = new List<Persona>();
            String sql = "SELECT id, cedula, nombre, edad, telefono, direccion FROM cs.persona " +
                "WHERE nombre = @nom";
            
            con.Open();
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
            cmd.Parameters.AddWithValue("@nom", val);

            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                Persona temp = Cargar(reader);
                val = temp.nombre;
                personas.Add(temp);
            }
            return null;

        }



        private Persona Cargar(NpgsqlDataReader reader)
        {
            Persona per = new Persona
            {
                id = reader.GetInt32(0),
                cedula = reader.GetString(1),
                nombre = reader.GetString(2),
                edad = reader.GetInt32(3),
                telefono = reader.GetInt32(4),
                direccion = reader.GetString(5)
            };
            return per;
        }

    }
}
