﻿using DAO;
using Entities;
using System;

namespace BO
{
    public class PersonaBO
    {
        private void Validar(Persona persona)
        {
            if (string.IsNullOrWhiteSpace(persona.cedula))
            {
                throw new Exception("Cedula Requerido");
            }
            if (string.IsNullOrWhiteSpace(persona.nombre))
            {
                throw new Exception("Nombre Requerido");
            }if (persona.telefono!=0)
            {
                throw new Exception("Telefono Requerido");
            }if (string.IsNullOrWhiteSpace(persona.direccion))
            {
                throw new Exception("Dirección Requerido");
            }
        }

        public void Registrar(Persona persona)
        {
            Validar(persona);
            if (persona.id > 0)
            {
                new PersonaDAO().ModificarPersona(persona);
            }
            else
            {
                new PersonaDAO().InsertarPersona(persona);
                //Console.WriteLine(persona.id);
            }
        }


    }
}
