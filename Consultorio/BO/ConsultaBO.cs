﻿using DAO;
using Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BO
{
    public class ConsultaBO
    {
        private void Validar(Consulta consulta)
        {
            if (consulta.precio==0)
            {
                throw new Exception("Precio Requerido");
            }
            if (consulta.numCita==0)
            {
                throw new Exception("Numero de Cita Requerido");
            }
            if (string.IsNullOrWhiteSpace(consulta.padecimiento))
            {
                throw new Exception("Padecimiento Requerido");
            }
            if (string.IsNullOrWhiteSpace(consulta.notas))
            {
                throw new Exception("Notas Requerido");
            }


        }

        public void Registrar(Consulta consulta)
        {
            Validar(consulta);
            if (consulta.idCita > 0)
            {
                new ConsultaDAO().ModificarConsulta(consulta);
            }
            else
            {
                new ConsultaDAO().InsertarConsulta(consulta);
                //Console.WriteLine(persona.id);
            }
        }


    }
}
