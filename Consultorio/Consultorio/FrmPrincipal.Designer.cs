﻿namespace Consultorio
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRegPersona = new System.Windows.Forms.Button();
            this.btnActualizar = new System.Windows.Forms.Button();
            this.btnRegCita = new System.Windows.Forms.Button();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnRegPersona
            // 
            this.btnRegPersona.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnRegPersona.Font = new System.Drawing.Font("Verdana", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegPersona.ForeColor = System.Drawing.Color.White;
            this.btnRegPersona.Location = new System.Drawing.Point(90, 46);
            this.btnRegPersona.Name = "btnRegPersona";
            this.btnRegPersona.Size = new System.Drawing.Size(148, 135);
            this.btnRegPersona.TabIndex = 0;
            this.btnRegPersona.Text = "Registrar Persona";
            this.btnRegPersona.UseVisualStyleBackColor = false;
            this.btnRegPersona.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnRegPersona_MouseClick);
            // 
            // btnActualizar
            // 
            this.btnActualizar.BackColor = System.Drawing.Color.Crimson;
            this.btnActualizar.Font = new System.Drawing.Font("Verdana", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActualizar.ForeColor = System.Drawing.Color.White;
            this.btnActualizar.Location = new System.Drawing.Point(357, 46);
            this.btnActualizar.Name = "btnActualizar";
            this.btnActualizar.Size = new System.Drawing.Size(148, 135);
            this.btnActualizar.TabIndex = 1;
            this.btnActualizar.Text = "Actualizar Cita";
            this.btnActualizar.UseVisualStyleBackColor = false;
            // 
            // btnRegCita
            // 
            this.btnRegCita.BackColor = System.Drawing.Color.Orange;
            this.btnRegCita.Font = new System.Drawing.Font("Verdana", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegCita.ForeColor = System.Drawing.Color.White;
            this.btnRegCita.Location = new System.Drawing.Point(90, 224);
            this.btnRegCita.Name = "btnRegCita";
            this.btnRegCita.Size = new System.Drawing.Size(148, 135);
            this.btnRegCita.TabIndex = 2;
            this.btnRegCita.Text = "Registrar Cita";
            this.btnRegCita.UseVisualStyleBackColor = false;
            // 
            // btnConsultar
            // 
            this.btnConsultar.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnConsultar.Font = new System.Drawing.Font("Verdana", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultar.ForeColor = System.Drawing.Color.White;
            this.btnConsultar.Location = new System.Drawing.Point(357, 224);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(148, 135);
            this.btnConsultar.TabIndex = 3;
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.UseVisualStyleBackColor = false;
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(605, 424);
            this.Controls.Add(this.btnConsultar);
            this.Controls.Add(this.btnRegCita);
            this.Controls.Add(this.btnActualizar);
            this.Controls.Add(this.btnRegPersona);
            this.Name = "FrmPrincipal";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnRegPersona;
        private System.Windows.Forms.Button btnActualizar;
        private System.Windows.Forms.Button btnRegCita;
        private System.Windows.Forms.Button btnConsultar;
    }
}

