﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class Consulta
    {
        public int numCita { get; set; }
        public int idCita { get; set; }
        public double precio { get; set; }
        public Persona persona { get; set; }
        public string padecimiento { get; set; }
        public string notas { get; set; }

        public Consulta(int numCita, int idCita, double precio, Persona persona, string padecimiento, string notas)
        {
            this.numCita = numCita;
            this.idCita = idCita;
            this.precio = precio;
            this.persona = persona;
            this.padecimiento = padecimiento;
            this.notas = notas;
        }

        public Consulta(int numCita, int idCita, double precio, string padecimiento, string notas)
        {
            this.numCita = numCita;
            this.idCita = idCita;
            this.precio = precio;
            this.padecimiento = padecimiento;
            this.notas = notas;
        }

        public Consulta()
        {
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
