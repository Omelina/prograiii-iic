﻿using System;

namespace Entities
{
    public class Persona
    {
        public int id { get; set; }
        public string cedula { get; set; }
        public string nombre { get; set; }
        public int edad { get; set; }
        public int telefono { get; set; }
        public string direccion { get; set; }

        public Persona(int id, string cedula, string nombre, int edad, int telefono, string direccion)
        {
            this.id = id;
            this.cedula = cedula;
            this.nombre = nombre;
            this.edad = edad;
            this.telefono = telefono;
            this.direccion = direccion;
        }

        public Persona()
        {
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
