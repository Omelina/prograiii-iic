Create database progratres
create schema cs

create table cs.persona(
	id serial primary key,
	cedula text not null,
	nombre text not null,
	edad int,
	telefono int not null,
	direccion text not null
	
);

create table cs.consulta(
	id serial primary key,
	precio float not null,
	num_cita int not null,
	padecimiento text not null,
	notas text,
	id_persona int references cs.persona(id) not null
)


drop table cs.consulta

select * from cs.persona