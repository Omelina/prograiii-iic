﻿using ProyDataGridV.DAL;
using ProyDataGridV.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyDataGridV.BOL
{
    public class EstudianteBO
    {

        private void Validar(Estudiante est)
        {
            if (string.IsNullOrEmpty(est.cedula))
            {
                throw new Exception("Cedula requerida");
            }
            if (string.IsNullOrEmpty(est.nombre))
            {
                throw new Exception("Nombre Requerido");
            }
            if (est.fechaNacimiento==null)
            {
                throw new Exception("Fecha Nacimiento Requerido");
            }
            if (string.IsNullOrEmpty(est.residencia))
            {
                throw new Exception("Residencia Requerido");
            }
        }

        public void Registrar(Estudiante est)
        {
            Validar(est);
            if (est.id > 0)
            {
                new BDEstudiante().ModificarEst(est);
            }
            else
            {
                new BDEstudiante().InsertarEst(est);
            }
        }

        public List<Estudiante> buscar()
        {
            return new BDEstudiante().CargarConsulta();
        }


    }
}
