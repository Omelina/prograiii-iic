﻿using ProyDataGridV.BOL;
using ProyDataGridV.DAL;
using ProyDataGridV.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyDataGridV
{
    public partial class Form1 : Form
    {
        Estudiante estudiante = new Estudiante();
        EstudianteBO ebo = new EstudianteBO();
        BDEstudiante edao = new BDEstudiante();

        public Form1()
        {
            InitializeComponent();
            dgvEst.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
        }

        private void cargarDatos()
        {

            EstudianteBO ebo = new EstudianteBO();
            dgvEst.AutoGenerateColumns = false;
            dgvEst.DataSource = null;
            dgvEst.DataSource = ebo.buscar();

        }


        private void dtpFechaNacimiento_ValueChanged(object sender, EventArgs e)
        {
            DateTime fNacimiento = dtpFechaNacimiento.Value;
            int edad = DateTime.Today.AddTicks(-fNacimiento.Ticks).Year - 1;
            txtEdad.Text = edad.ToString();
        }

        private void dgvEst_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvEst.CurrentCell.Value == null)
            {
                MessageBox.Show("Debe seleccionar un estudiante. ");
            }
            else
            {
                estudiante.id = (int)dgvEst.Rows[e.RowIndex].Cells[0].Value;
                estudiante.cedula = dgvEst.Rows[e.RowIndex].Cells[1].Value.ToString();
                estudiante.nombre = dgvEst.Rows[e.RowIndex].Cells[2].Value.ToString();
                estudiante.edad = (int)dgvEst.Rows[e.RowIndex].Cells[3].Value;
                estudiante.fechaNacimiento = (DateTime)dgvEst.Rows[e.RowIndex].Cells[4].Value;
                estudiante.residencia = dgvEst.Rows[e.RowIndex].Cells[5].Value.ToString();
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cargarDatos();
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            estudiante = new Estudiante
            {
                id = estudiante != null ? estudiante.id : 0,
                cedula = txtCed.Text,
                nombre = txtNombre.Text,
                edad = int.Parse(txtEdad.Text),
                fechaNacimiento = dtpFechaNacimiento.Value,
                residencia = txtResidencia.Text,
            };
            ebo.Registrar(estudiante);
            cargarDatos();
        }

        private void obtenerDatos()
        {
            txtCed.Text = estudiante.cedula;
            txtNombre.Text = estudiante.nombre;
            txtEdad.Text = estudiante.edad.ToString();
            dtpFechaNacimiento.Value = estudiante.fechaNacimiento;
            txtResidencia.Text = estudiante.residencia;
        }
        private void btnModificar_Click(object sender, EventArgs e)
        {
            obtenerDatos();
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtCed.Clear();
            txtNombre.Clear();
            txtEdad.Clear();
            dtpFechaNacimiento.ResetText();
            txtResidencia.Clear();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult con = MessageBox.Show("¿Está seguro que desea eliminar el estudiante?",
                        "Buscar Estudiante...", MessageBoxButtons.YesNo);
            if (con == DialogResult.No)
            {
                return;
            }
            else
            {
                edao.EliminarEst(estudiante);
                cargarDatos();
            }

        }
    }
}
