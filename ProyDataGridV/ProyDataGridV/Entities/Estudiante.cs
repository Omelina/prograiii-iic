﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyDataGridV.Entities
{
    public class Estudiante
    {
        public int id { get; set; }
        public string cedula { get; set; }
        public string nombre { get; set; }

        public int edad { get; set; }
        public DateTime fechaNacimiento { get; set; }
        public string residencia { get; set; }

        public Estudiante(int id, string cedula, string nombre, int edad, DateTime fechaNacimiento, string residencia)
        {
            this.id = id;
            this.cedula = cedula;
            this.nombre = nombre;
            this.edad = edad;
            this.fechaNacimiento = fechaNacimiento;
            this.residencia = residencia;
        }

        public Estudiante()
        {
        }

        public override string ToString()
        {
            return id + ", " + cedula + ", " + nombre + ", " +edad+", "+ fechaNacimiento + ", " + residencia;
        }
    }
}
