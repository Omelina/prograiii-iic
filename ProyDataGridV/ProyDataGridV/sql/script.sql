create database progratres
create schema pr

create table pr.Estudiante(
	id serial primary key,
	cedula text not null,
	nombre text not null,
	edad int not null,
	fecha_nacimiento date not null,
	residencia text not null,
	constraint unq_ced unique(cedula)
);

select * from pr.Estudiante