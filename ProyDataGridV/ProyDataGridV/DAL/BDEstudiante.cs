﻿using Npgsql;
using ProyDataGridV.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyDataGridV.DAL
{
    public class BDEstudiante
    {

        static NpgsqlConnection con;
        public void InsertarEst(Estudiante estudiante)
        {
            string sql = "INSERT INTO pr.estudiante(cedula, " +
                " nombre, edad, fecha_nacimiento, residencia)VALUES(@ced, @nom,@edad, @fna, @res); ";
            con = ClsConexion.Conexion();
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@ced", estudiante.cedula);
                cmd.Parameters.AddWithValue("@nom", estudiante.nombre);
                cmd.Parameters.AddWithValue("@edad", estudiante.edad);
                cmd.Parameters.AddWithValue("@fna", estudiante.fechaNacimiento);
                cmd.Parameters.AddWithValue("@res", estudiante.residencia);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public void ModificarEst(Estudiante estudiante)
        {
            string sql = "UPDATE pr.estudiante SET cedula = @ced, " +
                " nombre = @nom, edad = @edad, fecha_nacimiento = @fna, residencia = @res WHERE id = @id ";
            con = ClsConexion.Conexion();
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@ced", estudiante.cedula);
                cmd.Parameters.AddWithValue("@nom", estudiante.nombre);
                cmd.Parameters.AddWithValue("@edad", estudiante.edad);
                cmd.Parameters.AddWithValue("@fna", estudiante.fechaNacimiento);
                cmd.Parameters.AddWithValue("@res", estudiante.residencia);
                cmd.Parameters.AddWithValue("@id", estudiante.id);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public void EliminarEst(Estudiante estudiante)
        {
            string sql = "DELETE FROM pr.estudiante WHERE id = @id ";
            con = ClsConexion.Conexion();
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", estudiante.id);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }


        private Estudiante Cargar(NpgsqlDataReader reader)
        {
            Estudiante e = new Estudiante
            {
                id = reader.GetInt32(0),
                cedula = reader.GetString(1),
                nombre = reader.GetString(2),
                edad = reader.GetInt32(3),
                fechaNacimiento = reader.GetDateTime(4),
                residencia = reader.GetString(5)
            };
            return e;
        }

        public List<Estudiante> CargarConsulta()
        {
            con = ClsConexion.Conexion();
            con.Open();
            List<Estudiante> lista = new List<Estudiante>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, cedula, " +
                " nombre, edad, fecha_nacimiento, residencia FROM pr.estudiante; ", con);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(Cargar(dr));
                }
            }
            con.Close();
            return lista;

        }




    }
}
