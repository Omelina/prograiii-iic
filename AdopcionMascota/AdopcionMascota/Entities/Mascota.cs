﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdopcionMascota.Entities
{
    public class Mascota
    {
		public int id { get; set; }
		public string color { get; set; }
		public string tamano { get; set; }
		public char sexo { get; set; }
		public double edad { get; set; }
		public bool estado { get; set; }
		public DateTime fechaIngreso { get; set; }
		public Image foto { get; set; }

        public override string ToString()
        {
			return color + ", " + tamano + ", " + sexo + ", " + edad + ", " + fechaIngreso + ", "+foto;
        }
    }
}
