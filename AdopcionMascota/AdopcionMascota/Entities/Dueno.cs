﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdopcionMascota.Entities
{
    public class Dueno
    {
        public int id { get; set; }
        public string cedula { get; set; }
        public string nombre { get; set; }

        public Dueno(int id, string cedula, string nombre)
        {
            this.id = id;
            this.cedula = cedula;
            this.nombre = nombre;
        }

        public Dueno()
        {
        }
    

        public override string ToString()
        {
            return cedula + ", " + nombre;
        }
    }
}
