﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdopcionMascota.Entities
{
    public class DuenoMascota
    {
        public int id { get; set; }
        public string cedula { get; set; }
        public int idMascota { get; set; }
        public DateTime fechaAdopcion { get; set; }

        public override string ToString()
        {
            return cedula + ", " + idMascota + ", " + fechaAdopcion;
        }
    }
}
