﻿namespace AdopcionMascota
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dgvMascotas = new System.Windows.Forms.DataGridView();
            this.cId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cColor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cTamano = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cSexo = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cEdad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cEstado = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cFechaIngreso = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cFoto = new System.Windows.Forms.DataGridViewImageColumn();
            this.cRegistro = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.btnRegistrarA = new System.Windows.Forms.Button();
            this.dtpFA = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDCedula = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtDNombre = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pbFoto = new System.Windows.Forms.PictureBox();
            this.dtpFI = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtEdad = new System.Windows.Forms.TextBox();
            this.txtSexo = new System.Windows.Forms.TextBox();
            this.txtTamano = new System.Windows.Forms.TextBox();
            this.txtColor = new System.Windows.Forms.TextBox();
            this.listMascota = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDueño = new System.Windows.Forms.Button();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtCedula = new System.Windows.Forms.TextBox();
            this.listDueno = new System.Windows.Forms.ListBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txtBuscar = new System.Windows.Forms.TextBox();
            this.dgvAdopciones = new System.Windows.Forms.DataGridView();
            this.cAdoptados = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMascotas)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFoto)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAdopciones)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(3, 6);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1057, 572);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dgvMascotas);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1049, 543);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Lista de mascotas";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dgvMascotas
            // 
            this.dgvMascotas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMascotas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cId,
            this.cColor,
            this.cTamano,
            this.cSexo,
            this.cEdad,
            this.cEstado,
            this.cFechaIngreso,
            this.cFoto,
            this.cRegistro});
            this.dgvMascotas.Location = new System.Drawing.Point(0, 3);
            this.dgvMascotas.Name = "dgvMascotas";
            this.dgvMascotas.RowHeadersWidth = 51;
            this.dgvMascotas.RowTemplate.Height = 24;
            this.dgvMascotas.Size = new System.Drawing.Size(1046, 537);
            this.dgvMascotas.TabIndex = 0;
            this.dgvMascotas.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dgvMascotas.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvMascotas_DataError);
            // 
            // cId
            // 
            this.cId.DataPropertyName = "id";
            this.cId.HeaderText = "Id";
            this.cId.MinimumWidth = 6;
            this.cId.Name = "cId";
            this.cId.Visible = false;
            this.cId.Width = 125;
            // 
            // cColor
            // 
            this.cColor.HeaderText = "Color";
            this.cColor.MinimumWidth = 6;
            this.cColor.Name = "cColor";
            this.cColor.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cColor.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.cColor.Width = 125;
            // 
            // cTamano
            // 
            this.cTamano.HeaderText = "Tamaño";
            this.cTamano.MinimumWidth = 6;
            this.cTamano.Name = "cTamano";
            this.cTamano.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cTamano.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.cTamano.Width = 125;
            // 
            // cSexo
            // 
            this.cSexo.FalseValue = "";
            this.cSexo.HeaderText = "Sexo";
            this.cSexo.MinimumWidth = 6;
            this.cSexo.Name = "cSexo";
            this.cSexo.TrueValue = "";
            this.cSexo.Width = 125;
            // 
            // cEdad
            // 
            this.cEdad.DataPropertyName = "edad";
            this.cEdad.HeaderText = "Edad";
            this.cEdad.MinimumWidth = 6;
            this.cEdad.Name = "cEdad";
            this.cEdad.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cEdad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.cEdad.Width = 125;
            // 
            // cEstado
            // 
            this.cEstado.DataPropertyName = "estado";
            this.cEstado.HeaderText = "Estado";
            this.cEstado.MinimumWidth = 6;
            this.cEstado.Name = "cEstado";
            this.cEstado.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cEstado.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.cEstado.Visible = false;
            this.cEstado.Width = 125;
            // 
            // cFechaIngreso
            // 
            this.cFechaIngreso.DataPropertyName = "fechaIngreso";
            this.cFechaIngreso.HeaderText = "Fecha Ingreso";
            this.cFechaIngreso.MinimumWidth = 6;
            this.cFechaIngreso.Name = "cFechaIngreso";
            this.cFechaIngreso.Width = 125;
            // 
            // cFoto
            // 
            this.cFoto.HeaderText = "Foto";
            this.cFoto.MinimumWidth = 6;
            this.cFoto.Name = "cFoto";
            this.cFoto.Width = 125;
            // 
            // cRegistro
            // 
            this.cRegistro.HeaderText = "Registrar/Guardar";
            this.cRegistro.MinimumWidth = 6;
            this.cRegistro.Name = "cRegistro";
            this.cRegistro.Text = "Guardar";
            this.cRegistro.UseColumnTextForButtonValue = true;
            this.cRegistro.Width = 125;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Controls.Add(this.listMascota);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.btnDueño);
            this.tabPage2.Controls.Add(this.txtNombre);
            this.tabPage2.Controls.Add(this.txtCedula);
            this.tabPage2.Controls.Add(this.listDueno);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1049, 543);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Nueva Adopción";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnLimpiar);
            this.groupBox2.Controls.Add(this.btnRegistrarA);
            this.groupBox2.Controls.Add(this.dtpFA);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtDCedula);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.txtDNombre);
            this.groupBox2.Location = new System.Drawing.Point(337, 283);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(408, 235);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Información del Dueño";
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(39, 191);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(134, 27);
            this.btnLimpiar.TabIndex = 20;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnRegistrarA
            // 
            this.btnRegistrarA.Location = new System.Drawing.Point(230, 191);
            this.btnRegistrarA.Name = "btnRegistrarA";
            this.btnRegistrarA.Size = new System.Drawing.Size(147, 27);
            this.btnRegistrarA.TabIndex = 19;
            this.btnRegistrarA.Text = "Registrar Adopción";
            this.btnRegistrarA.UseVisualStyleBackColor = true;
            this.btnRegistrarA.Click += new System.EventHandler(this.btnRegistrarA_Click);
            // 
            // dtpFA
            // 
            this.dtpFA.CustomFormat = "dd/MM/yyyy";
            this.dtpFA.Enabled = false;
            this.dtpFA.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFA.Location = new System.Drawing.Point(178, 119);
            this.dtpFA.Name = "dtpFA";
            this.dtpFA.Size = new System.Drawing.Size(148, 22);
            this.dtpFA.TabIndex = 16;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(58, 121);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(114, 17);
            this.label10.TabIndex = 15;
            this.label10.Text = "Fecha Adopción:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(98, 85);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 17);
            this.label7.TabIndex = 18;
            this.label7.Text = "Nombre:";
            // 
            // txtDCedula
            // 
            this.txtDCedula.Enabled = false;
            this.txtDCedula.Location = new System.Drawing.Point(178, 42);
            this.txtDCedula.Name = "txtDCedula";
            this.txtDCedula.Size = new System.Drawing.Size(149, 22);
            this.txtDCedula.TabIndex = 15;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(104, 47);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 17);
            this.label9.TabIndex = 17;
            this.label9.Text = "Cedula:";
            // 
            // txtDNombre
            // 
            this.txtDNombre.Enabled = false;
            this.txtDNombre.Location = new System.Drawing.Point(177, 82);
            this.txtDNombre.Name = "txtDNombre";
            this.txtDNombre.Size = new System.Drawing.Size(149, 22);
            this.txtDNombre.TabIndex = 16;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pbFoto);
            this.groupBox1.Controls.Add(this.dtpFI);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtEdad);
            this.groupBox1.Controls.Add(this.txtSexo);
            this.groupBox1.Controls.Add(this.txtTamano);
            this.groupBox1.Controls.Add(this.txtColor);
            this.groupBox1.Location = new System.Drawing.Point(337, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(408, 265);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Información de Mascota";
            // 
            // pbFoto
            // 
            this.pbFoto.Location = new System.Drawing.Point(261, 42);
            this.pbFoto.Name = "pbFoto";
            this.pbFoto.Size = new System.Drawing.Size(141, 145);
            this.pbFoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbFoto.TabIndex = 14;
            this.pbFoto.TabStop = false;
            // 
            // dtpFI
            // 
            this.dtpFI.CustomFormat = "dd/MM/yyyy";
            this.dtpFI.Enabled = false;
            this.dtpFI.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFI.Location = new System.Drawing.Point(137, 208);
            this.dtpFI.Name = "dtpFI";
            this.dtpFI.Size = new System.Drawing.Size(148, 22);
            this.dtpFI.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 213);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(102, 17);
            this.label8.TabIndex = 12;
            this.label8.Text = "Fecha Ingreso:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(36, 160);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 17);
            this.label6.TabIndex = 9;
            this.label6.Text = "Edad:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(38, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Sexo:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "Tamaño:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(36, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Color:";
            // 
            // txtEdad
            // 
            this.txtEdad.Enabled = false;
            this.txtEdad.Location = new System.Drawing.Point(94, 157);
            this.txtEdad.Name = "txtEdad";
            this.txtEdad.Size = new System.Drawing.Size(100, 22);
            this.txtEdad.TabIndex = 3;
            // 
            // txtSexo
            // 
            this.txtSexo.Enabled = false;
            this.txtSexo.Location = new System.Drawing.Point(94, 115);
            this.txtSexo.Name = "txtSexo";
            this.txtSexo.Size = new System.Drawing.Size(100, 22);
            this.txtSexo.TabIndex = 2;
            // 
            // txtTamano
            // 
            this.txtTamano.Enabled = false;
            this.txtTamano.Location = new System.Drawing.Point(94, 75);
            this.txtTamano.Name = "txtTamano";
            this.txtTamano.Size = new System.Drawing.Size(149, 22);
            this.txtTamano.TabIndex = 1;
            // 
            // txtColor
            // 
            this.txtColor.Enabled = false;
            this.txtColor.Location = new System.Drawing.Point(94, 37);
            this.txtColor.Name = "txtColor";
            this.txtColor.Size = new System.Drawing.Size(149, 22);
            this.txtColor.TabIndex = 0;
            // 
            // listMascota
            // 
            this.listMascota.FormattingEnabled = true;
            this.listMascota.ItemHeight = 16;
            this.listMascota.Location = new System.Drawing.Point(21, 19);
            this.listMascota.Name = "listMascota";
            this.listMascota.Size = new System.Drawing.Size(302, 500);
            this.listMascota.TabIndex = 7;
            this.listMascota.Click += new System.EventHandler(this.listMascota_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(754, 429);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Nombre:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(760, 385);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Cedula:";
            // 
            // btnDueño
            // 
            this.btnDueño.Location = new System.Drawing.Point(892, 474);
            this.btnDueño.Name = "btnDueño";
            this.btnDueño.Size = new System.Drawing.Size(140, 28);
            this.btnDueño.TabIndex = 3;
            this.btnDueño.Text = "Registar Dueño";
            this.btnDueño.UseVisualStyleBackColor = true;
            this.btnDueño.Click += new System.EventHandler(this.btnDueño_Click);
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(822, 426);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(210, 22);
            this.txtNombre.TabIndex = 2;
            // 
            // txtCedula
            // 
            this.txtCedula.Location = new System.Drawing.Point(822, 385);
            this.txtCedula.Name = "txtCedula";
            this.txtCedula.Size = new System.Drawing.Size(210, 22);
            this.txtCedula.TabIndex = 1;
            // 
            // listDueno
            // 
            this.listDueno.FormattingEnabled = true;
            this.listDueno.ItemHeight = 16;
            this.listDueno.Location = new System.Drawing.Point(763, 19);
            this.listDueno.Name = "listDueno";
            this.listDueno.Size = new System.Drawing.Size(270, 340);
            this.listDueno.TabIndex = 0;
            this.listDueno.Click += new System.EventHandler(this.listDueno_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnBuscar);
            this.tabPage3.Controls.Add(this.txtBuscar);
            this.tabPage3.Controls.Add(this.dgvAdopciones);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1049, 543);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Lista de Adopciones";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(507, 34);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(134, 27);
            this.btnBuscar.TabIndex = 2;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // txtBuscar
            // 
            this.txtBuscar.Location = new System.Drawing.Point(23, 39);
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(468, 22);
            this.txtBuscar.TabIndex = 1;
            // 
            // dgvAdopciones
            // 
            this.dgvAdopciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAdopciones.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cAdoptados});
            this.dgvAdopciones.Location = new System.Drawing.Point(8, 100);
            this.dgvAdopciones.Name = "dgvAdopciones";
            this.dgvAdopciones.RowHeadersWidth = 51;
            this.dgvAdopciones.RowTemplate.Height = 24;
            this.dgvAdopciones.Size = new System.Drawing.Size(1029, 429);
            this.dgvAdopciones.TabIndex = 0;
            // 
            // cAdoptados
            // 
            this.cAdoptados.HeaderText = "Adopciones";
            this.cAdoptados.MinimumWidth = 6;
            this.cAdoptados.Name = "cAdoptados";
            this.cAdoptados.Width = 1000;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1059, 580);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Adopción de Mascotas";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMascotas)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFoto)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAdopciones)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dgvMascotas;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridViewTextBoxColumn cId;
        private System.Windows.Forms.DataGridViewTextBoxColumn cColor;
        private System.Windows.Forms.DataGridViewTextBoxColumn cTamano;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cSexo;
        private System.Windows.Forms.DataGridViewTextBoxColumn cEdad;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cEstado;
        private System.Windows.Forms.DataGridViewTextBoxColumn cFechaIngreso;
        private System.Windows.Forms.DataGridViewImageColumn cFoto;
        private System.Windows.Forms.DataGridViewButtonColumn cRegistro;
        private System.Windows.Forms.ListBox listMascota;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnDueño;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtCedula;
        private System.Windows.Forms.ListBox listDueno;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtTamano;
        private System.Windows.Forms.TextBox txtColor;
        private System.Windows.Forms.TextBox txtEdad;
        private System.Windows.Forms.TextBox txtSexo;
        private System.Windows.Forms.DateTimePicker dtpFI;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pbFoto;
        private System.Windows.Forms.Button btnRegistrarA;
        private System.Windows.Forms.DateTimePicker dtpFA;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtDCedula;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtDNombre;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dgvAdopciones;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox txtBuscar;
        private System.Windows.Forms.DataGridViewTextBoxColumn cAdoptados;
    }
}

