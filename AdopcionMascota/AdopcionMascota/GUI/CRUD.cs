﻿using AdopcionMascota.BOL;
using AdopcionMascota.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdopcionMascota
{
    public partial class Form1 : Form
    {
        DateTimePicker dtp;
        Mascota m = new Mascota();
        Dueno d = new Dueno();
        DateTime fecha;
        byte[] img;
        Image imagen;
        public Form1()
        {
            InitializeComponent();
        }

        private void cargarAdopcion()
        {
            string filtro = txtBuscar.Text.Trim();
            DuenoMascotaBOL mbol = new DuenoMascotaBOL();
            dgvAdopciones.AutoGenerateColumns = false;
            dgvAdopciones.Rows.Clear();
            foreach (var item in mbol.bucarAdopcion(filtro))
            {
                dgvAdopciones.Rows.Add(item);

            }

        }

        private void cargarDatos()
        {
            MascotaBOL mbo = new MascotaBOL();
            dgvMascotas.AutoGenerateColumns = false;
            dgvMascotas.Rows.Clear();
            foreach (var item in mbo.buscar())
            {
                if (item.estado == true)
                {
                    dgvMascotas.Rows.Add(item.id, item.color,
                    item.tamano, item.sexo == 'F' ? cSexo.Selected = true :
                    cSexo.Selected = false, item.edad, item.estado,
                    fecha = item.fechaIngreso, img);
                }
                
            }

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex==8)
            {
                m.id = (int)dgvMascotas.Rows[e.RowIndex].Cells[0].Value;
                m.color = dgvMascotas.Rows[e.RowIndex].Cells[1].Value.ToString();
                m.tamano = dgvMascotas.Rows[e.RowIndex].Cells[2].Value.ToString();
                if ((bool)dgvMascotas.Rows[e.RowIndex].Cells[3].Value==true)
                {
                    m.sexo = 'F';
                }
                else
                {
                    m.sexo = 'M';
                }
                double result;
                if( double.TryParse(dgvMascotas.Rows[e.RowIndex].Cells[4].Value.ToString(),out result))
                {
                    m.edad = result;
                }
                m.estado = (bool)dgvMascotas.Rows[e.RowIndex].Cells[5].Value;
                m.fechaIngreso = dtp.Value.Date;
                m.foto = imagen;
                MascotaBOL cbo = new MascotaBOL();
                cbo.Registrar(m);

            }
            if (e.ColumnIndex==6)
            {
                // initialize DateTimePicker
                dtp = new DateTimePicker();
                dtp.Format = DateTimePickerFormat.Short;
                dtp.Visible = true;
                dtp.Value = fecha.Date;


                // set size and location
                var rect = dgvMascotas.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);
                dtp.Size = new Size(rect.Width, rect.Height);
                dtp.Location = new Point(rect.X, rect.Y);

                // attach events
                dtp.CloseUp += new EventHandler(dtp_CloseUp);
                dtp.TextChanged += new EventHandler(dtp_OnTextChange);

                dgvMascotas.Controls.Add(dtp);
            }
            if (e.ColumnIndex == 7)
            {
                OpenFileDialog fd = new OpenFileDialog();
                fd.Filter = "Choose Image(*.jpg;*.png;*.gif) |*.jpg;*.png;*.gif";
                if (fd.ShowDialog() == DialogResult.OK)
                {
                    pbFoto.Image = Image.FromFile(fd.FileName);
                    try
                    {
                        MemoryStream ms = new MemoryStream();
                        pbFoto.Image.Save(ms, pbFoto.Image.RawFormat);
                        img = ms.ToArray();
                        imagen = Image.FromStream(ms);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    cargarDatos();
                }
            }
        }

        private void dtp_OnTextChange(object sender, EventArgs e)
        {
            dgvMascotas.CurrentCell.Value = dtp.Text.ToString();
        }

        private void dtp_CloseUp(object sender, EventArgs e)
        {
            dtp.Visible = false;
        }

        private void cargarLista()
        {
            MascotaBOL mbo = new MascotaBOL();
            listMascota.DataSource = null;
            foreach (var item in mbo.buscar())
            {
                if (item.estado == true)
                {
                    listMascota.DataSource = mbo.buscar();
                }
            }
            
            DuenoBOL dbo = new DuenoBOL();
            listDueno.DataSource = null;
            listDueno.DataSource = dbo.cargar();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            cargarDatos();
            cargarLista();
            cargarAdopcion();
        }

        private void dgvMascotas_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if(e.Exception.Message == "El Valor del DataGridViewComboBoxCell no es válido")
            {
                object value = dgvMascotas.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                if (!((DataGridViewComboBoxColumn)dgvMascotas.Columns[e.ColumnIndex]).Items.Contains(value))
                {
                    ((DataGridViewComboBoxColumn)dgvMascotas.Columns[e.ColumnIndex]).Items.Add(value);
                    e.ThrowException = false;
                }
            }
        }

        private void listMascota_Click(object sender, EventArgs e)
        {
            if(listMascota.SelectedIndex != -1)
            {
                MascotaBOL mbo = new MascotaBOL();
                foreach (var item in mbo.buscar())
                {
                    if (item.id == listMascota.SelectedIndex + 1)
                    {
                        txtColor.Text = item.color;
                        txtTamano.Text = item.tamano;
                        txtSexo.Text = char.ConvertFromUtf32(item.sexo);
                        txtEdad.Text = item.edad.ToString();
                        dtpFI.Value = item.fechaIngreso;
                        pbFoto.Image = item.foto;
                        m.id = item.id;
                        m.color = item.color;
                        m.tamano = item.tamano;
                        m.sexo = item.sexo;
                        m.edad = item.edad;
                        m.estado = item.estado;
                        m.fechaIngreso = item.fechaIngreso;
                        m.foto = item.foto;
                    }
                }
            }
            else
            {
                MessageBox.Show("Debe escoger una mascota");
            }
            
        }

        private void listDueno_Click(object sender, EventArgs e)
        {
            if (listMascota.SelectedIndex != -1)
            {
                DuenoBOL dbo = new DuenoBOL();
                foreach (var item in dbo.cargar())
                {
                    if (item.id == listDueno.SelectedIndex+1)
                    {
                        txtDCedula.Text = item.cedula;
                        txtDNombre.Text = item.nombre;
                        d.id = item.id;
                        d.cedula = item.cedula;
                        d.nombre = item.nombre;
                    }
                }
            }
            else
            {
                MessageBox.Show("Debe escoger una mascota");
            }
        }

        private void btnDueño_Click(object sender, EventArgs e)
        {
            
            DuenoBOL dbo = new DuenoBOL();
            foreach (var item in dbo.cargar())
            {
                if (item.cedula== txtCedula.Text)
                {
                    MessageBox.Show("La persona ya ha sido registrada");
                }
                else
                {
                    d.cedula = txtCedula.Text;
                    d.nombre = txtNombre.Text;
                    dbo.Registrar(d);
                    cargarLista();
                    txtCedula.Clear();
                    txtNombre.Clear();
                }
            }
        }

        private void btnRegistrarA_Click(object sender, EventArgs e)
        {
            DuenoMascotaBOL dmbol = new DuenoMascotaBOL();
            if(d!=null && m != null)
            {
                DuenoMascota dm = new DuenoMascota
                {
                    cedula = d.cedula,
                    idMascota = m.id,
                    fechaAdopcion = dtpFA.Value
                };
                dmbol.Registrar(dm);
                m.estado = false;
                new MascotaBOL().Registrar(m);
                cargarDatos();
                cargarLista();
            }
            else
            {
                MessageBox.Show("No se ha seleccionado un dueño o una mascota");
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtColor.Clear();txtDCedula.Clear();txtDNombre.Clear(); txtTamano.Clear();
            txtSexo.Clear();txtEdad.Clear(); dtpFI.Value = DateTime.Now; dtpFA.Value = DateTime.Now;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            String filtro = txtBuscar.Text.Trim();
            if (filtro == "")
            {
                DialogResult con = MessageBox.Show("¿Está seguro que desea cargar todas las adopciones?",
                        "Buscar Adopciones...", MessageBoxButtons.YesNo);
                if (con == DialogResult.No)
                {
                    return;
                }
            }
            cargarAdopcion();

        }

    }
}
