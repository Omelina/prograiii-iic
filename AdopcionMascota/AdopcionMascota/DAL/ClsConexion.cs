﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdopcionMascota.DAL
{
    public class ClsConexion
    {
        static NpgsqlConnection con;


        public static NpgsqlConnection Conexion()
        {
            con = new NpgsqlConnection("Server = localhost; User id = postgres; Password = postgres; Database = adopta");
            return con;
        }

    }
}
