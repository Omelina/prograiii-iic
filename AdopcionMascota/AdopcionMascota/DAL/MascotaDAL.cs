﻿using AdopcionMascota.Entities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdopcionMascota.DAL
{
    public class MascotaDAL
    {
        static NpgsqlConnection con;
        public void InsertarMas(Mascota mascota)
        {
            string sql = "INSERT INTO public.mascota(color, tamano, sexo, " +
                " edad, estado, fecha_ingreso, foto VALUES( @col, @tam, @sexo, @edad, @est, @fing, @foto); ";
            con = ClsConexion.Conexion();
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@col", mascota.color);
                cmd.Parameters.AddWithValue("@tam", mascota.tamano);
                cmd.Parameters.AddWithValue("@sexo", mascota.sexo);
                cmd.Parameters.AddWithValue("@edad", mascota.edad);
                cmd.Parameters.AddWithValue("@est", mascota.estado);
                cmd.Parameters.AddWithValue("@fing", mascota.fechaIngreso);
                cmd.Parameters.AddWithValue("@foto", mascota.foto);
                
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public void ModificarMas(Mascota mascota)
        {
            string sql = "UPDATE public.mascota SET " +
                " color = @col, tamano = @tam, sexo = @sexo, edad = @edad, " +
                " estado = @est, fecha_ingreso = @fing WHERE id= @id ";
            con = ClsConexion.Conexion();
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@col", mascota.color);
                cmd.Parameters.AddWithValue("@tam", mascota.tamano);
                cmd.Parameters.AddWithValue("@sexo", mascota.sexo);
                cmd.Parameters.AddWithValue("@edad", mascota.edad);
                cmd.Parameters.AddWithValue("@est", mascota.estado);
                cmd.Parameters.AddWithValue("@fing", mascota.fechaIngreso);
                //cmd.Parameters.AddWithValue("@foto", mascota.foto);
                cmd.Parameters.AddWithValue("@id", mascota.id);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        private Mascota Cargar(NpgsqlDataReader reader)
        {
            Mascota m = new Mascota();
            m.id = reader.GetInt32(0);
            m.color = reader.GetString(1);
            m.tamano = reader.GetString(2);
            m.sexo = reader.GetChar(3);
            m.edad = reader.GetDouble(4);
            m.estado = reader.GetBoolean(5);
            m.fechaIngreso = reader.GetDateTime(6);
            byte[] bytes = (byte[])reader[7];
            m.foto = ByteImage(bytes);
            return m;
        }

        private Image ByteImage(byte[] bytes)
        {
            //Image returnImage = null;
            //using (MemoryStream ms = new MemoryStream(bytes))
            //{
            //    returnImage = Image.FromStream(ms);
            //}
            //return returnImage;
            if (bytes == null) return null;
            MemoryStream ms = new MemoryStream(bytes);
            Bitmap bm = null;
            try
            {
                bm = new Bitmap(ms);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            return bm;
        }

        public List<Mascota> CargarConsulta()
        {
            con = ClsConexion.Conexion();
            con.Open();
            List<Mascota> lista = new List<Mascota>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, color, " +
                " tamano, sexo, edad, estado, fecha_ingreso, foto FROM public.mascota ", con);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(Cargar(dr));
                }
            }
            con.Close();
            return lista;

        }

        

    }
}
