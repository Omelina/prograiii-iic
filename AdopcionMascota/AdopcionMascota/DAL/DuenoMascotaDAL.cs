﻿using AdopcionMascota.Entities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdopcionMascota.DAL
{
    public class DuenoMascotaDAL
    {
        static NpgsqlConnection con;
        public void InsertarDM(DuenoMascota dm)
        {
            string sql = "INSERT INTO public.mascota_dueno(ced_dueno, id_mascota, " +
                " fecha_adopcion) VALUES(@cedd, @idm, @fadp); ";
            con = ClsConexion.Conexion();
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@cedd", dm.cedula);
                cmd.Parameters.AddWithValue("@idm", dm.idMascota);
                cmd.Parameters.AddWithValue("@fadp", dm.fechaAdopcion);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public List<String> CargarAdopcion(string text)
        {
            con = ClsConexion.Conexion();
            con.Open();
            List<String> lista = new List<String>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT m.id, d.nombre, color, tamano, sexo, edad, fecha_adopcion " +
                " FROM public.mascota_dueno md inner join mascota m on md.id_mascota = m.id " +
                " inner join dueno d on md.ced_dueno = d.cedula " +
                " WHERE lower(d.nombre) like lower(@nom) order by d.nombre", con);
            cmd.Parameters.AddWithValue("@nom", text + '%');
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(Cargar(dr));
                }
            }
            con.Close();
            return lista;
        }

        private String Cargar(NpgsqlDataReader reader)
        {
            string consulta = "";
            consulta += reader.GetInt32(0);
            consulta += " Nombre: "+reader.GetString(1);
            consulta += "   Color: "+reader.GetString(2);
            consulta += "   Tamaño: "+reader.GetString(3);
            consulta += "   Sexo: "+reader.GetChar(4);
            consulta += "   Edad: " + reader.GetDouble(5);
            consulta += "   Fecha de Adopcion: " + reader.GetDateTime(6);
            return consulta;

        }


    }
}
