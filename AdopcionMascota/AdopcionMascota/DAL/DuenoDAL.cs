﻿using AdopcionMascota.Entities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdopcionMascota.DAL
{
    class DuenoDAL
    {
        static NpgsqlConnection con;
        public void InsertarDueno(Dueno dueno)
        {
            string sql = "INSERT INTO public.dueno(cedula, nombre) VALUES(@ced, @nom) ";
            con = ClsConexion.Conexion();
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@ced", dueno.cedula);
                cmd.Parameters.AddWithValue("@nom", dueno.nombre);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public void ModificarDueno(Dueno dueno)
        {
            string sql = "UPDATE public.dueno SET cedula = @ced, nombre = @nom WHERE id = @id ";
            con = ClsConexion.Conexion();
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@ced", dueno.cedula);
                cmd.Parameters.AddWithValue("@nom", dueno.nombre);
                cmd.Parameters.AddWithValue("@id", dueno.id);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        private Dueno Cargar(NpgsqlDataReader reader)
        {
            Dueno d = new Dueno
            {
                id = reader.GetInt32(0),
                cedula = reader.GetString(1),
                nombre = reader.GetString(2),
            };
            return d;
        }

        

        public List<Dueno> CargarDuenos()
        {
            con = ClsConexion.Conexion();
            con.Open();
            List<Dueno> lista = new List<Dueno>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, cedula, nombre FROM public.dueno" +
                " order by id", con);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(Cargar(dr));
                }
            }
            con.Close();
            return lista;

        }
    }
}
