﻿using AdopcionMascota.DAL;
using AdopcionMascota.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdopcionMascota.BOL
{
    public class MascotaBOL
    {
        private void Validar(Mascota mascota)
        {
            if (string.IsNullOrEmpty(mascota.color))
            {
                throw new Exception("Color requerida");
            }
            if (string.IsNullOrEmpty(mascota.tamano))
            {
                throw new Exception("Tamaño Requerido");
            }
            if (mascota.edad == 0)
            {
                throw new Exception("Edad Requerido");
            }
            if (mascota.fechaIngreso == null)
            {
                throw new Exception("Fecha de Ingreso Requerido");
            }

        }

        public void Registrar(Mascota mascota)
        {
            Validar(mascota);
            if (mascota.id > 0)
            {
                new MascotaDAL().ModificarMas(mascota);
            }
            else
            {
                new MascotaDAL().InsertarMas(mascota);
            }
        }

        

        public List<Mascota> buscar()
        {
            return new MascotaDAL().CargarConsulta();
        }


    }

}
