﻿using AdopcionMascota.DAL;
using AdopcionMascota.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdopcionMascota.BOL
{
    class DuenoMascotaBOL
    {

        public void Registrar(DuenoMascota dm)
        {
            new DuenoMascotaDAL().InsertarDM(dm);
        }

        public List<string> bucarAdopcion(string text)
        {
            return new DuenoMascotaDAL().CargarAdopcion(text);
        }

    }
}
