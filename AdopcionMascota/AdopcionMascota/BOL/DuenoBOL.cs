﻿using AdopcionMascota.DAL;
using AdopcionMascota.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdopcionMascota.BOL
{
    class DuenoBOL
    {
        private void Validar(Dueno dueno)
        {
            if (String.IsNullOrEmpty(dueno.cedula))
            {
                throw new Exception("Cedula requerida");
            }if (String.IsNullOrEmpty(dueno.nombre))
            {
                throw new Exception("Nombre requerido");
            }

        }

        public void Registrar(Dueno dueno)
        {
            Validar(dueno);
            new DuenoDAL().InsertarDueno(dueno);
        }

        

        public List<Dueno> cargar()
        {
            return new DuenoDAL().CargarDuenos();
        }
    }
}
