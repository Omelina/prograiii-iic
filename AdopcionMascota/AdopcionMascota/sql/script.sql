create database adopta

create table mascota (
	id serial primary key,
	color text not null,
	tamano text not null,
	sexo char default 'M',
	edad double precision,
	estado boolean default true,
	fecha_ingreso date not null,
	foto bytea default E'\\000'
);

create table dueno(
	id serial primary key,
	cedula text not null,
	nombre text not null,
	constraint unq_ced unique(cedula)
)
drop table mascota
drop table dueno
drop table mascota_dueno

create table mascota_dueno(
	id serial not null,
	ced_dueno text not null,
	id_mascota int not null,
	fecha_adopcion date not null,
	constraint fk_ced_dueno foreign key(ced_dueno) references public.dueno(cedula), 
    constraint fk_id_mascota foreign key(id_mascota) references public.mascota(id)
)

INSERT INTO public.mascota(color, tamano, sexo, edad, estado, fecha_ingreso)
	VALUES ( 'negro', 'mediano', 'F', 5, true, '24/05/2020');
select * from mascota
select * from dueno
select * from mascota_dueno

INSERT INTO public.dueno(cedula, nombre)
	VALUES ('207810510','Oky Madrigal');
INSERT INTO public.mascota_dueno(ced_dueno, id_mascota, fecha_adopcion)
	VALUES ('207810510', 1, '26/06/2020');

SELECT m.id, d.nombre, color, tamano, sexo, edad, md.fecha_adopcion FROM public.mascota m
inner join mascota_dueno md on md.id_mascota = m.id
inner join dueno d on md.ced_dueno = d.cedula WHERE lower(d.nombre) like lower('Francisco') order by d.nombre

SELECT m.id, d.nombre, color, tamano, sexo, edad, fecha_adopcion
FROM public.mascota_dueno md inner join mascota m on md.id_mascota = m.id
inner join dueno d on md.ced_dueno = d.cedula WHERE lower(d.nombre) like lower('Francisco') order by d.nombre
	
	
UPDATE public.mascota  SET estado = true WHERE id=1
