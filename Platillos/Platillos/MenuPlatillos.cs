﻿using Platillos.DAL;
using Platillos.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics.PerformanceData;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Platillos
{
    public partial class MenuPlatillos : Form
    {

        PlatilloDAL pdal = new PlatilloDAL();
        string ruta = "Datos.xml";

        public MenuPlatillos()
        {
            InitializeComponent();
            cargarDatos();
        }

        private void cargarDatos()
        {
            dgvPlatillos.Rows.Clear();
            if (pdal.LeerXml() != null)
            {
                foreach (Platillo a in pdal.LeerXml())
                {
                    dgvPlatillos.Rows.Add(a.id, a.nombre, a.precio, a.descripcion, a.calorias);
                    txtNPlatillo.Text = (a.id+1).ToString();
                }
            }

        }

        private void limpiar()
        {
            txtCalorías.Clear();
            txtDescripción.Clear();
            txtNombre.Clear();
            txtPrecio.Clear();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            Platillo p = new Platillo();
            p.id = Int32.Parse(txtNPlatillo.Text.Trim());
            p.nombre = txtNombre.Text.Trim();
            p.precio = double.Parse(txtPrecio.Text.Trim());
            p.descripcion = txtDescripción.Text.Trim();
            p.calorias = Int32.Parse(txtCalorías.Text.Trim());
            foreach(var item in pdal.LeerXml())
            {
                if (item.nombre == p.nombre)
                {
                    MessageBox.Show("No se puede agregar el platillo" +
                        " porque ya está repetido");
                    p = null;
                    limpiar();
                    break;
                }
            }
            if (p != null)
            {
                pdal.AgregarDatos(p);
                int id = Int32.Parse(txtNPlatillo.Text.Trim());
                id++;
                txtNPlatillo.Text = id.ToString();
                limpiar();
                cargarDatos();
            }
            
        }


        private void btnCargar_Click_1(object sender, EventArgs e)
        {
            cargarDatos();
        }

        private void btnCrearXml_Click(object sender, EventArgs e)
        {
            pdal.CrearXML(ruta, "Platillos");
        }

        private void btnReporte_Click(object sender, EventArgs e)
        {
            dgvPlatillos.Rows.Clear();
            
            foreach (Platillo p in pdal.LeerXml())
            {
                if (p.calorias>=450 && p.calorias<= 700)
                {
                    dgvPlatillos.Rows.Add(p.id, p.nombre, p.precio, p.descripcion, p.calorias);
                    for (int i = 0; i < dgvPlatillos.Rows.Count; i++)
                    {
                        if (i > 0)
                        {
                            dgvPlatillos.Rows[i - 1].Visible = false;
                            dgvPlatillos.Rows[0].Visible = true;
                            dgvPlatillos.Rows[1].Visible = true;
                        }

                    }
                }
            }
        }
    }
}
