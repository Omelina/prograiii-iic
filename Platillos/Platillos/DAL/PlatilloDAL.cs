﻿using Platillos.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Platillos.DAL
{
    class PlatilloDAL
    {
        XmlDocument doc;
        string rutaXml;
        public void CrearXML(string ruta, string nodoRaiz)
        {
            doc = new XmlDocument();
            rutaXml = ruta;

            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", "no");

            XmlNode nodo = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, nodo);

            XmlNode elemento = doc.CreateElement(nodoRaiz);
            doc.AppendChild(elemento);

            doc.Save(ruta);
        }

        private XmlNode CrearPlatillo(Platillo p)
        {
            XmlNode platillo = doc.CreateElement("plato");

            XmlElement xid = doc.CreateElement("id");
            xid.InnerText = p.id.ToString();
            platillo.AppendChild(xid);

            XmlElement xnombre = doc.CreateElement("nombre");
            xnombre.InnerText = p.nombre;
            platillo.AppendChild(xnombre);

            XmlElement xprecio = doc.CreateElement("precio");
            xprecio.InnerText = p.precio.ToString();
            platillo.AppendChild(xprecio);

            XmlElement xdescripcion = doc.CreateElement("descripcion");
            xdescripcion.InnerText = p.descripcion;
            platillo.AppendChild(xdescripcion);

            XmlElement xcalorias = doc.CreateElement("calorias");
            xcalorias.InnerText = p.calorias.ToString();
            platillo.AppendChild(xcalorias);

            return platillo;
        }

        public void AgregarDatos(Platillo platillo)
        {
            string ruta = "Datos.xml";
            doc.Load(ruta);

            XmlNode plato = CrearPlatillo(platillo);

            XmlNode nodo = doc.DocumentElement;

            nodo.InsertAfter(plato, nodo.LastChild);

            doc.Save(ruta);
        }

        public List<Platillo> LeerXml()
        {
            doc = new XmlDocument();
            string ruta = "Datos.xml";
            doc.Load(ruta);

            XmlNodeList listaPlatillos = doc.SelectNodes("Platillos/plato");

            XmlNode unPlatillo;
            List<Platillo> lista = new List<Platillo>();
            for (int i = 0; i < listaPlatillos.Count; i++)
            {
                unPlatillo = listaPlatillos.Item(i);
                Platillo pl = new Platillo();
                pl.id = Int32.Parse( unPlatillo.SelectSingleNode("id").InnerText);
                pl.nombre = unPlatillo.SelectSingleNode("nombre").InnerText;
                pl.precio = double.Parse(unPlatillo.SelectSingleNode("precio").InnerText);
                pl.descripcion = unPlatillo.SelectSingleNode("descripcion").InnerText;
                pl.calorias = Int32.Parse( unPlatillo.SelectSingleNode("calorias").InnerText);
                lista.Add(pl);
                
            }
            return lista;
        }


    }
}
