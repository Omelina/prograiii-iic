﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platillos.Entidades
{
    public class Platillo
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public double precio { get; set; }
        public string descripcion { get; set; }
        public int calorias { get; set; }

        public override string ToString()
        {
            return id + ", " + nombre + ", " + precio + ", " + descripcion + ", " + calorias;
        }
    }
}
