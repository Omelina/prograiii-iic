﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empresa.Entities
{
    public class Colaborador
    {
        public int id { get; set; }
        public string cedula { get; set; }
        public string nombre { get; set; }
        public char genero { get; set; }
        public Departamento idDep { get; set; }
        public DateTime fechaNacimiento { get; set; }
        public int edad { get; set; }
        public DateTime fechaIngreso { get; set; }
        public string nivelIngles { get; set; }
        public Puesto idPuesto { get; set; }

        public Colaborador(int id, string cedula, string nombre, char genero,
            Departamento idDep, DateTime fechaNacimiento, int edad, 
            DateTime fechaIngreso, string nivelIngles, 
            Puesto idPuesto)
        {
            this.id = id;
            this.cedula = cedula;
            this.nombre = nombre;
            this.genero = genero;
            this.idDep = idDep;
            this.fechaNacimiento = fechaNacimiento;
            this.edad = edad;
            this.fechaIngreso = fechaIngreso;
            this.nivelIngles = nivelIngles;
            this.idPuesto = idPuesto;
        }

        public Colaborador()
        {
        }


        public override string ToString()
        {
            return id + ", " + cedula + ", " + nombre + ", " + genero + ", " + idDep + ", " + fechaNacimiento +
                ", " + edad + ", " + fechaIngreso + ", " + nivelIngles + ", " + idPuesto;
        }
    }
}
