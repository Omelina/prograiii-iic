﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empresa.Entities
{
    public class Puesto
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string abreviacion { get; set; }

        public Puesto(int id, string nombre, string abreviacion)
        {
            this.id = id;
            this.nombre = nombre;
            this.abreviacion = abreviacion;
        }

        public Puesto()
        {
        }

        public override string ToString()
        {
            return nombre + ", " + abreviacion;
        }
    }
}
