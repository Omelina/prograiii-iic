﻿using Empresa.DAL;
using Empresa.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empresa.BOL
{
    class ColaboradorBOLcs
    {
        private void Validar(Colaborador colab)
        {
            if (string.IsNullOrEmpty(colab.cedula))
            {
                throw new Exception("Cedula requerida");
            }
            if (string.IsNullOrEmpty(colab.nombre))
            {
                throw new Exception("Nombre Requerido");
            }
            if (char.IsWhiteSpace(colab.genero))
            {
                throw new Exception("Genero Requerido");
            }
            if (colab.idDep == null)
            {
                throw new Exception("Departamento Requerido");
            }
            if (colab.fechaNacimiento == null)
            {
                throw new Exception("Fecha de nacimiento Requerido");
            }
            if (colab.fechaIngreso == null)
            {
                throw new Exception("Fecha de Ingreso Requerido");
            }
            if (string.IsNullOrEmpty(colab.nivelIngles))
            {
                throw new Exception("Nivel de Inglés Requerido");
            }if (colab.idPuesto == null)
            {
                throw new Exception("Puesto Requerido");
            }


        }

        public void Registrar(Colaborador colab)
        {
            Validar(colab);
            if (colab.id > 0)
            {
                new ColaboradorDAL().ModificarColab(colab);
            }
            else
            {
                new ColaboradorDAL().InsertarColab(colab);
            }
        }

        public List<Colaborador> buscar(String filtro)
        {
            return new ColaboradorDAL().CargarConsulta(filtro);
        }

        public List<Colaborador> Filtro1(String ingles,string genero)
        {
            return new ColaboradorDAL().Filtro1(ingles, genero);
        }

        public List<Colaborador> Filtro2(String f1,string f2)
        {
            return new ColaboradorDAL().Filtro2(f1, f2);
        }

        public int cargarDato(int edad1,int edad2)
        {
            return new ColaboradorDAL().Filtro3(edad1, edad2);
        }


    }
}
