﻿using Empresa.DAL;
using Empresa.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empresa.BOL
{
    class PuestoBOL
    {

        public List<Puesto> buscar()
        {
            return new PuestoDAL().CargarConsulta();
        }

        public Puesto cargarDato(string id)
        {
            return new PuestoDAL().CargarString(id);
        }
    }
}
