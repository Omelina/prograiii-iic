﻿using Empresa.DAL;
using Empresa.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empresa.BOL
{
    class DepartamentoBOL
    {
        public List<Departamento> buscar()
        {
            return new DepartamentoDAL().CargarConsulta();
        }

        public Departamento cargarDato(int id)
        {
            return new DepartamentoDAL().CargarString(id);
        }

    }
}
