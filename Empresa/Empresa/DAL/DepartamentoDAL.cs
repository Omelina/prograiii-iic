﻿using Empresa.Entities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empresa.DAL
{
    class DepartamentoDAL
    {
        static NpgsqlConnection con;

        private Departamento Cargar(NpgsqlDataReader reader)
        {
            Departamento d = new Departamento
            {
                id = reader.GetInt32(0),
                nombre = reader.GetString(1),

            };
            return d;
        }

        public List<Departamento> CargarConsulta()
        {
            con = ClsConexion.Conexion();
            con.Open();
            List<Departamento> lista = new List<Departamento>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, nombre FROM public.departamento ",con);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(Cargar(dr));
                }
            }
            con.Close();
            return lista;

        }

        public Departamento CargarString(int id)
        {
            con = ClsConexion.Conexion();
            con.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, nombre FROM public.departamento " +
                " WHERE id = @id",con);
            cmd.Parameters.AddWithValue("@id", id);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    return Cargar(dr);
                }
            }
            con.Close();
            return null;
        }

        public Departamento CargarDep(int id)
        {
            con = ClsConexion.Conexion();
            con.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("select d.id, d.nombre FROM public.departamento d " +
                " right join colaborador c on c.id_departamento = d.id " +
                " WHERE c.id = @id", con);
            cmd.Parameters.AddWithValue("@id", id);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    return Cargar(dr);
                }
            }
            con.Close();
            return null;
        }
    }
}
