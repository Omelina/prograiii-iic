﻿using Empresa.Entities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empresa.DAL
{
    public class ColaboradorDAL
    {
        static NpgsqlConnection con;
        public void InsertarColab(Colaborador colaborador)
        {
            string sql = "INSERT INTO public.colaborador(cedula," +
                " nombre, genero, id_departamento, fecha_nacimiento, edad, " +
                " fecha_ingreso, nivel_ingles, id_puesto)VALUES(@ced, @nom, @gen, @idd, " +
                " @fna, @edad, @fing, @niv, @idp)";
            con = ClsConexion.Conexion();
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@ced", colaborador.cedula);
                cmd.Parameters.AddWithValue("@nom", colaborador.nombre);
                cmd.Parameters.AddWithValue("@gen", colaborador.genero);
                cmd.Parameters.AddWithValue("@idd", colaborador.idDep.id);
                cmd.Parameters.AddWithValue("@fna", colaborador.fechaIngreso);
                cmd.Parameters.AddWithValue("@edad", colaborador.edad);
                cmd.Parameters.AddWithValue("@fing", colaborador.fechaIngreso);
                cmd.Parameters.AddWithValue("@niv", colaborador.nivelIngles);
                cmd.Parameters.AddWithValue("@idp", colaborador.idPuesto.id);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public void ModificarColab(Colaborador colaborador)
        {
            string sql = "UPDATE public.colaborador SET cedula =@ced, " +
                " nombre =@nom, genero =@gen, id_departamento =@idd, fecha_nacimiento =@fna, " +
                " edad =@edad, fecha_ingreso =@fing, nivel_ingles =@niv, id_puesto =@idp WHERE id = @id ";
            con = ClsConexion.Conexion();
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@ced", colaborador.cedula);
                cmd.Parameters.AddWithValue("@nom", colaborador.nombre);
                cmd.Parameters.AddWithValue("@gen", colaborador.genero);
                cmd.Parameters.AddWithValue("@idd", colaborador.idDep.id);
                cmd.Parameters.AddWithValue("@fna", colaborador.fechaNacimiento);
                cmd.Parameters.AddWithValue("@edad", colaborador.edad);
                cmd.Parameters.AddWithValue("@fing", colaborador.fechaIngreso);
                cmd.Parameters.AddWithValue("@niv", colaborador.nivelIngles);
                cmd.Parameters.AddWithValue("@idp", colaborador.idPuesto.id);
                cmd.Parameters.AddWithValue("@id", colaborador.id);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        private Colaborador Cargar(NpgsqlDataReader reader)
        {
            Colaborador c = new Colaborador
            {
                id = reader.GetInt32(0),
                cedula = reader.GetString(1),
                nombre = reader.GetString(2),
                genero = reader.GetChar(3),
                idDep = new DepartamentoDAL().CargarDep(reader.GetInt32(0)),
                fechaNacimiento = reader.GetDateTime(5),
                edad = reader.GetInt32(6),
                fechaIngreso = reader.GetDateTime(7),
                nivelIngles = reader.GetString(8),
                idPuesto = new PuestoDAL().CargarPuesto(reader.GetInt32(0))
            };
            return c;
        }

        public List<Colaborador> CargarConsulta(string texto)
        {
            con = ClsConexion.Conexion();
            con.Open();
            List<Colaborador> lista = new List<Colaborador>();
            NpgsqlCommand cmd = new NpgsqlCommand("Select co.id, cedula, " +
                " co.nombre, genero, b.nombre, fecha_nacimiento, edad, " +
                " fecha_ingreso, nivel_ingles, p.abreviacion from " +
                " colaborador co inner join departamento b on " +
                " co.id_departamento = b.id inner join puesto p on co.id_puesto = p.id" +
                " WHERE lower(cedula) like lower(@ced) or lower(b.nombre) " +
                " like lower(@dno)", con);
            cmd.Parameters.AddWithValue("@ced", texto + '%');
            cmd.Parameters.AddWithValue("@dno", texto + '%');
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(Cargar(dr));
                }
            }
            con.Close();
            return lista;

        }

        public List<Colaborador> Filtro1(string ingles, string genero)
        {
            con = ClsConexion.Conexion();
            con.Open();
            List<Colaborador> lista = new List<Colaborador>();
            NpgsqlCommand cmd = new NpgsqlCommand("Select co.id, cedula, " +
                " co.nombre, genero, b.nombre, fecha_nacimiento, edad, " +
                " fecha_ingreso, nivel_ingles, p.abreviacion from " +
                " colaborador co inner join departamento b on " +
                " co.id_departamento = b.id inner join puesto p on co.id_puesto = p.id" +
                " WHERE lower(genero) like lower(@gen) " +
                " and lower(nivel_ingles) like lower(@niv) ", con);
            cmd.Parameters.AddWithValue("@gen", genero + '%');
            cmd.Parameters.AddWithValue("@niv", ingles + '%');
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(Cargar(dr));
                }
            }
            con.Close();
            return lista;
        }

        public List<Colaborador> Filtro2(string fe1, string fe2)
        {
            con = ClsConexion.Conexion();
            con.Open();
            List<Colaborador> lista = new List<Colaborador>();
            NpgsqlCommand cmd = new NpgsqlCommand("Select co.id, cedula, " +
                " co.nombre, genero, b.nombre, fecha_nacimiento, edad, " +
                " fecha_ingreso, nivel_ingles, p.abreviacion from " +
                " colaborador co inner join departamento b on " +
                " co.id_departamento = b.id inner join puesto p on co.id_puesto = p.id" +
                " WHERE fecha_ingreso BETWEEN CAST (@fe1 AS DATE) AND CAST (@fe2 AS DATE)", con);
            cmd.Parameters.AddWithValue("@fe1", fe1 + '%');
            cmd.Parameters.AddWithValue("@fe2", fe2 + '%');
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(Cargar(dr));
                }
            }
            con.Close();
            return lista;
        }

        public int Filtro3(int edad1, int edad2)
        {
            int text=0;
            con = ClsConexion.Conexion();
            con.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("select count(edad) " +
                " from colaborador where edad between @ed1 and @ed2", con);
            cmd.Parameters.AddWithValue("@ed1", edad1);
            cmd.Parameters.AddWithValue("@ed2", edad2);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    return text = dr.GetInt32(0);
                }
            }
            con.Close();
            return 0;
        }

    }
}
