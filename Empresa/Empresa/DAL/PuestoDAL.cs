﻿using Empresa.Entities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empresa.DAL
{
    class PuestoDAL
    {
        static NpgsqlConnection con;

        private Puesto Cargar(NpgsqlDataReader reader)
        {
            Puesto p = new Puesto
            {
                id = reader.GetInt32(0),
                nombre = reader.GetString(1),
                abreviacion = reader.GetString(2),

            };
            return p;
        }

        public List<Puesto> CargarConsulta()
        {
            con = ClsConexion.Conexion();
            con.Open();
            List<Puesto> lista = new List<Puesto>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, nombre, abreviacion FROM public.puesto ",con);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(Cargar(dr));
                }
            }
            con.Close();
            return lista;

        }

        public Puesto CargarString(string id)
        {
            con = ClsConexion.Conexion();
            con.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, nombre, abreviacion FROM public.puesto" +
                " WHERE id = @id",con);
            cmd.Parameters.AddWithValue("@id", id + '%');
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    return Cargar(dr);
                }
            }
            con.Close();
            return null;
        }

        public Puesto CargarPuesto(int id)
        {
            con = ClsConexion.Conexion();
            con.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT p.id, p.nombre, abreviacion FROM public.puesto p " +
                " right join colaborador c on c.id_puesto = p.id " +
                " WHERE c.id = @id", con);
            cmd.Parameters.AddWithValue("@id", id);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    return Cargar(dr);
                }
            }
            con.Close();
            return null;
        }
    }
}
