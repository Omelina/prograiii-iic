﻿using Empresa.BOL;
using Empresa.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Empresa.GUI
{
    public partial class FrmConsulta : Form
    {
        Colaborador colaborador = new Colaborador();
        private int edades { get; set; }
        private int edades2 { get; set; }
        
        public FrmConsulta()
        {
            InitializeComponent();
            dgvColab.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
        }

        private void cargarDatos(string filtro)
        {
            
            ColaboradorBOLcs cbo = new ColaboradorBOLcs();
            dgvColab.AutoGenerateColumns = false;
            dgvColab.DataSource = null;
            dgvColab.DataSource = cbo.buscar(filtro);
            
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            String filtro = txtFiltro.Text.Trim();
            if (filtro == "")
            {
                DialogResult con = MessageBox.Show("¿Está seguro que desea cargar todos los empleados?",
                        "Buscar Colaboradores...", MessageBoxButtons.YesNo);
                if (con == DialogResult.No)
                {
                    return;
                }
            }
            cargarDatos(filtro);
            cargarLista();
        }

        private void cargarLista()
        {
            List<int> edades = new List<int>();
            List<int> edades2 = new List<int>();
            for (int i = 18; i < 99; i++)
            {
                edades.Add(i);
                
            }
            for (int f = 18; f < 99; f++)
            {
                edades2.Add(f);
            }
            cbxEdades.DataSource = edades;
            cbxEdades2.DataSource = edades2;
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            String filtro = txtFiltro.Text.Trim();
            if (dgvColab.SelectedRows == null)
            {
                MessageBox.Show("Debe seleccionar un empleado");
            }
            else{
                FrmRegistro frm = new FrmRegistro(colaborador);
                Hide();
                frm.ShowDialog();
                Show();
                cargarDatos(filtro);
            }
            
        }

        private void dgvColab_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvColab.CurrentCell.Value == null)
            {
                MessageBox.Show("Debe seleccionar un empleado. " +
                    "Si desea cargar la lista seleccione el botón Buscar");
            }
            else
            {
                colaborador.id = (int)dgvColab.Rows[e.RowIndex].Cells[0].Value;
                colaborador.cedula = dgvColab.Rows[e.RowIndex].Cells[1].Value.ToString();
                colaborador.nombre = dgvColab.Rows[e.RowIndex].Cells[2].Value.ToString();
                colaborador.genero = (char)dgvColab.Rows[e.RowIndex].Cells[3].Value;
                colaborador.idDep = (Departamento)dgvColab.Rows[e.RowIndex].Cells[4].Value;
                colaborador.fechaNacimiento = (DateTime)dgvColab.Rows[e.RowIndex].Cells[5].Value;
                colaborador.edad = (int)dgvColab.Rows[e.RowIndex].Cells[6].Value;
                colaborador.fechaIngreso = (DateTime)dgvColab.Rows[e.RowIndex].Cells[7].Value;
                colaborador.nivelIngles = dgvColab.Rows[e.RowIndex].Cells[8].Value.ToString();
                colaborador.idPuesto = (Puesto)dgvColab.Rows[e.RowIndex].Cells[9].Value;
            }
            
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            String filtro = txtFiltro.Text.Trim();
            FrmRegistro frm = new FrmRegistro(null);
            Hide();
            frm.ShowDialog();
            Show();
            cargarDatos(filtro);
        }

        private void btnFiltro1_Click(object sender, EventArgs e)
        {
            string ingles = cbxIngles.Text;
            string genero = cbxGenero.Text;
            

            if (ingles==""||genero==""){

                MessageBox.Show("Para buscar por este filtro " +
                    " debe escoger un sexo y un nivel de inglés");
            }
            else
            {
                ColaboradorBOLcs cbo = new ColaboradorBOLcs();
                dgvColab.DataSource = null;
                dgvColab.DataSource = cbo.Filtro1(ingles,genero);
            }
            
        }

        private void btnFechas_Click(object sender, EventArgs e)
        {
            string f1 = dtf1.Value.ToString("yyyy-MM-dd",CultureInfo.InvariantCulture);
            string f2 = dtf2.Value.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            if (dtf1.Value > dtf2.Value)
            {
                MessageBox.Show("La fecha 1 no puede ser mayor a la segunda");
            }
            else
            {
                ColaboradorBOLcs cbo = new ColaboradorBOLcs();
                dgvColab.DataSource = null;
                dgvColab.DataSource = cbo.Filtro2(f1,f2);
            }
        }

        private void cbxEdades_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            edades = int.Parse(cbxEdades.Text);
        }

        private void cbxEdades2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ColaboradorBOLcs cbo = new ColaboradorBOLcs();
            edades2 = int.Parse(cbxEdades2.Text);
            txtEdades.Text = cbo.cargarDato(edades, edades2).ToString();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }

}
