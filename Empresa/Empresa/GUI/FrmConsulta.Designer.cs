﻿namespace Empresa.GUI
{
    partial class FrmConsulta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtFiltro = new System.Windows.Forms.TextBox();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.dgvColab = new System.Windows.Forms.DataGridView();
            this.cId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cCedula = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cGenero = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cDepartamento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cFechaN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cEdad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cFechaIng = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cNivelIng = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPuesto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnEditar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.btnFiltro1 = new System.Windows.Forms.Button();
            this.cbxIngles = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbxGenero = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtf1 = new System.Windows.Forms.DateTimePicker();
            this.dtf2 = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cbxEdades = new System.Windows.Forms.ComboBox();
            this.cbxEdades2 = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtEdades = new System.Windows.Forms.TextBox();
            this.btnFechas = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvColab)).BeginInit();
            this.SuspendLayout();
            // 
            // txtFiltro
            // 
            this.txtFiltro.Location = new System.Drawing.Point(39, 28);
            this.txtFiltro.Name = "txtFiltro";
            this.txtFiltro.Size = new System.Drawing.Size(471, 22);
            this.txtFiltro.TabIndex = 0;
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(531, 24);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(77, 26);
            this.btnBuscar.TabIndex = 1;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // dgvColab
            // 
            this.dgvColab.ColumnHeadersHeight = 29;
            this.dgvColab.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cId,
            this.cCedula,
            this.cNombre,
            this.cGenero,
            this.cDepartamento,
            this.cFechaN,
            this.cEdad,
            this.cFechaIng,
            this.cNivelIng,
            this.cPuesto});
            this.dgvColab.Location = new System.Drawing.Point(22, 84);
            this.dgvColab.Name = "dgvColab";
            this.dgvColab.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dgvColab.RowTemplate.Height = 24;
            this.dgvColab.Size = new System.Drawing.Size(872, 357);
            this.dgvColab.TabIndex = 2;
            this.dgvColab.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvColab_CellClick);
            // 
            // cId
            // 
            this.cId.DataPropertyName = "id";
            this.cId.HeaderText = "id";
            this.cId.MinimumWidth = 6;
            this.cId.Name = "cId";
            this.cId.Visible = false;
            this.cId.Width = 6;
            // 
            // cCedula
            // 
            this.cCedula.DataPropertyName = "cedula";
            this.cCedula.HeaderText = "Cedula";
            this.cCedula.MinimumWidth = 6;
            this.cCedula.Name = "cCedula";
            this.cCedula.Width = 75;
            // 
            // cNombre
            // 
            this.cNombre.DataPropertyName = "nombre";
            this.cNombre.HeaderText = "Nombre";
            this.cNombre.MinimumWidth = 6;
            this.cNombre.Name = "cNombre";
            this.cNombre.Width = 125;
            // 
            // cGenero
            // 
            this.cGenero.DataPropertyName = "genero";
            this.cGenero.HeaderText = "Genero";
            this.cGenero.MinimumWidth = 6;
            this.cGenero.Name = "cGenero";
            this.cGenero.Width = 65;
            // 
            // cDepartamento
            // 
            this.cDepartamento.DataPropertyName = "idDep";
            this.cDepartamento.HeaderText = "Departamento";
            this.cDepartamento.MinimumWidth = 6;
            this.cDepartamento.Name = "cDepartamento";
            this.cDepartamento.Width = 115;
            // 
            // cFechaN
            // 
            this.cFechaN.DataPropertyName = "fechaNacimiento";
            this.cFechaN.HeaderText = "Fecha Nacimiento";
            this.cFechaN.MinimumWidth = 8;
            this.cFechaN.Name = "cFechaN";
            this.cFechaN.Width = 125;
            // 
            // cEdad
            // 
            this.cEdad.DataPropertyName = "edad";
            this.cEdad.HeaderText = "Edad";
            this.cEdad.MinimumWidth = 6;
            this.cEdad.Name = "cEdad";
            this.cEdad.Width = 50;
            // 
            // cFechaIng
            // 
            this.cFechaIng.DataPropertyName = "FechaIngreso";
            this.cFechaIng.HeaderText = "Fecha Ingreso";
            this.cFechaIng.MinimumWidth = 6;
            this.cFechaIng.Name = "cFechaIng";
            this.cFechaIng.Width = 125;
            // 
            // cNivelIng
            // 
            this.cNivelIng.DataPropertyName = "nivelIngles";
            this.cNivelIng.HeaderText = "Inglés";
            this.cNivelIng.MinimumWidth = 6;
            this.cNivelIng.Name = "cNivelIng";
            this.cNivelIng.Width = 55;
            // 
            // cPuesto
            // 
            this.cPuesto.DataPropertyName = "idPuesto";
            this.cPuesto.HeaderText = "Puesto";
            this.cPuesto.MinimumWidth = 6;
            this.cPuesto.Name = "cPuesto";
            this.cPuesto.Width = 125;
            // 
            // btnEditar
            // 
            this.btnEditar.Location = new System.Drawing.Point(911, 211);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(77, 26);
            this.btnEditar.TabIndex = 3;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = true;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(911, 257);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(77, 26);
            this.btnEliminar.TabIndex = 4;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.Location = new System.Drawing.Point(911, 170);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(77, 26);
            this.btnRegistrar.TabIndex = 5;
            this.btnRegistrar.Text = "Registar";
            this.btnRegistrar.UseVisualStyleBackColor = true;
            this.btnRegistrar.Click += new System.EventHandler(this.btnRegistrar_Click);
            // 
            // btnFiltro1
            // 
            this.btnFiltro1.Location = new System.Drawing.Point(500, 463);
            this.btnFiltro1.Name = "btnFiltro1";
            this.btnFiltro1.Size = new System.Drawing.Size(95, 45);
            this.btnFiltro1.TabIndex = 6;
            this.btnFiltro1.Tag = "Mujeres con cierto nivel de inglés";
            this.btnFiltro1.Text = "Busqueda por filtro";
            this.btnFiltro1.UseVisualStyleBackColor = true;
            this.btnFiltro1.Click += new System.EventHandler(this.btnFiltro1_Click);
            // 
            // cbxIngles
            // 
            this.cbxIngles.FormattingEnabled = true;
            this.cbxIngles.Items.AddRange(new object[] {
            "A1",
            "A2",
            "B1",
            "B2",
            "C1"});
            this.cbxIngles.Location = new System.Drawing.Point(71, 485);
            this.cbxIngles.Name = "cbxIngles";
            this.cbxIngles.Size = new System.Drawing.Size(102, 24);
            this.cbxIngles.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(73, 465);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 17);
            this.label1.TabIndex = 8;
            this.label1.Text = "Nivel de inglés";
            // 
            // cbxGenero
            // 
            this.cbxGenero.FormattingEnabled = true;
            this.cbxGenero.Items.AddRange(new object[] {
            "M",
            "F"});
            this.cbxGenero.Location = new System.Drawing.Point(191, 485);
            this.cbxGenero.Name = "cbxGenero";
            this.cbxGenero.Size = new System.Drawing.Size(64, 24);
            this.cbxGenero.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(190, 465);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 10;
            this.label2.Text = "Género";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 488);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "Filtros";
            // 
            // dtf1
            // 
            this.dtf1.CustomFormat = "dd-MM-yyyy";
            this.dtf1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtf1.Location = new System.Drawing.Point(369, 509);
            this.dtf1.Name = "dtf1";
            this.dtf1.Size = new System.Drawing.Size(95, 22);
            this.dtf1.TabIndex = 12;
            // 
            // dtf2
            // 
            this.dtf2.CustomFormat = "dd-MM-yyyy";
            this.dtf2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtf2.Location = new System.Drawing.Point(369, 542);
            this.dtf2.Name = "dtf2";
            this.dtf2.Size = new System.Drawing.Size(95, 22);
            this.dtf2.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(296, 465);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(180, 41);
            this.label4.TabIndex = 14;
            this.label4.Text = "Buscar por rango de fecha de ingreso";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(306, 514);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 17);
            this.label5.TabIndex = 15;
            this.label5.Text = "Fecha 1:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(306, 547);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 17);
            this.label6.TabIndex = 16;
            this.label6.Text = "Fecha 2:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(645, 524);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 17);
            this.label7.TabIndex = 21;
            this.label7.Text = "Edad 2:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(645, 491);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 17);
            this.label8.TabIndex = 20;
            this.label8.Text = "Edad 1:";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(645, 466);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(180, 18);
            this.label9.TabIndex = 19;
            this.label9.Text = "Buscar por rango de edad";
            // 
            // cbxEdades
            // 
            this.cbxEdades.FormattingEnabled = true;
            this.cbxEdades.Location = new System.Drawing.Point(719, 488);
            this.cbxEdades.Name = "cbxEdades";
            this.cbxEdades.Size = new System.Drawing.Size(106, 24);
            this.cbxEdades.TabIndex = 22;
            this.cbxEdades.SelectedIndexChanged += new System.EventHandler(this.cbxEdades_SelectedIndexChanged);
            // 
            // cbxEdades2
            // 
            this.cbxEdades2.FormattingEnabled = true;
            this.cbxEdades2.Location = new System.Drawing.Point(719, 524);
            this.cbxEdades2.Name = "cbxEdades2";
            this.cbxEdades2.Size = new System.Drawing.Size(106, 24);
            this.cbxEdades2.TabIndex = 23;
            this.cbxEdades2.SelectedIndexChanged += new System.EventHandler(this.cbxEdades2_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(852, 466);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(121, 52);
            this.label10.TabIndex = 24;
            this.label10.Text = "Cantidad de personas con ese rango";
            // 
            // txtEdades
            // 
            this.txtEdades.Location = new System.Drawing.Point(855, 526);
            this.txtEdades.Name = "txtEdades";
            this.txtEdades.Size = new System.Drawing.Size(109, 22);
            this.txtEdades.TabIndex = 25;
            // 
            // btnFechas
            // 
            this.btnFechas.Location = new System.Drawing.Point(500, 520);
            this.btnFechas.Name = "btnFechas";
            this.btnFechas.Size = new System.Drawing.Size(95, 44);
            this.btnFechas.TabIndex = 26;
            this.btnFechas.Text = "Buscar por fechas";
            this.btnFechas.UseVisualStyleBackColor = true;
            this.btnFechas.Click += new System.EventHandler(this.btnFechas_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(919, 11);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(68, 28);
            this.btnSalir.TabIndex = 27;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // FrmConsulta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 583);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnFechas);
            this.Controls.Add(this.txtEdades);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.cbxEdades2);
            this.Controls.Add(this.cbxEdades);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dtf2);
            this.Controls.Add(this.dtf1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbxGenero);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbxIngles);
            this.Controls.Add(this.btnFiltro1);
            this.Controls.Add(this.btnRegistrar);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.dgvColab);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.txtFiltro);
            this.Name = "FrmConsulta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consulta de Empleados";
            ((System.ComponentModel.ISupportInitialize)(this.dgvColab)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtFiltro;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.DataGridView dgvColab;
        private System.Windows.Forms.Button btnEditar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnRegistrar;
        private System.Windows.Forms.Button btnFiltro1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cId;
        private System.Windows.Forms.DataGridViewTextBoxColumn cCedula;
        private System.Windows.Forms.DataGridViewTextBoxColumn cNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn cGenero;
        private System.Windows.Forms.DataGridViewTextBoxColumn cDepartamento;
        private System.Windows.Forms.DataGridViewTextBoxColumn cFechaN;
        private System.Windows.Forms.DataGridViewTextBoxColumn cEdad;
        private System.Windows.Forms.DataGridViewTextBoxColumn cFechaIng;
        private System.Windows.Forms.DataGridViewTextBoxColumn cNivelIng;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPuesto;
        private System.Windows.Forms.ComboBox cbxIngles;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbxGenero;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtf1;
        private System.Windows.Forms.DateTimePicker dtf2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbxEdades;
        private System.Windows.Forms.ComboBox cbxEdades2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtEdades;
        private System.Windows.Forms.Button btnFechas;
        private System.Windows.Forms.Button btnSalir;
    }
}