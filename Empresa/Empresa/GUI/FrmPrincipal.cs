﻿using Empresa.GUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Empresa
{
    public partial class FrmPrincipal : Form
    {
        public FrmPrincipal()
        { 
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FrmRegistro frm = new FrmRegistro(null);
            Hide();
            frm.ShowDialog();
            Show();

        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            FrmConsulta frm = new FrmConsulta();
            Hide();
            frm.ShowDialog();
            Show();
        }
    }
}
