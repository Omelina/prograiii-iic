﻿using Empresa.BOL;
using Empresa.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Empresa.GUI
{
    public partial class FrmRegistro : Form
    {
        Colaborador colaborador { get; set; }
        DepartamentoBOL dbo = new DepartamentoBOL();
        PuestoBOL pbo = new PuestoBOL();
        ColaboradorBOLcs cbo = new ColaboradorBOLcs();
        public FrmRegistro(Colaborador col)
        {
            InitializeComponent();
            colaborador = col;
        }


        private void cargardatos()
        {
            txtCedula.Text = colaborador.cedula.ToString();
            txtNombre.Text = colaborador.nombre;
            if (colaborador.genero == 'F')
            {
                rbtFem.Checked = true;
            }
            else
            {
                rbtM.Checked = true;
            }
            dtNacim.Value = colaborador.fechaNacimiento;
            txtEdad.Text = colaborador.edad.ToString();
            dtIngreso.Value = colaborador.fechaIngreso;
            cbxIngles.Text = colaborador.nivelIngles;

        }

        private void FrmRegistro_Load(object sender, EventArgs e)
        {
            DepartamentoBOL dbo = new DepartamentoBOL();
            cbxDep.DataSource = dbo.buscar();
            //cbxDep.Items.Add(dbo.buscar());
            PuestoBOL pbo = new PuestoBOL();
            cbxPuesto.DataSource = pbo.buscar();
            //cbxPuesto.Items.Add(pbo.buscar());
            if (colaborador == null)
            {
                colaborador = new Colaborador();
            }
            else
            {
                cargardatos();
            }
            
        }

        private void dtNacim_ValueChanged(object sender, EventArgs e)
        {
            DateTime fechaActual = DateTime.Today;
            int edad = fechaActual.Year - dtNacim.Value.Year;
            if (fechaActual < dtNacim.Value.AddYears(edad)); edad--;
            txtEdad.Text = edad.ToString();
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            colaborador = new Colaborador
            {
                id = colaborador != null ? colaborador.id : 0,
                cedula = txtCedula.Text,
                nombre = txtNombre.Text,
                genero = rbtFem.Checked == true ? colaborador.genero = 'F':colaborador.genero='M',
                idDep = (Departamento)cbxDep.SelectedItem,
                fechaNacimiento = dtNacim.Value,
                edad = Int32.Parse(txtEdad.Text),
                fechaIngreso = dtIngreso.Value,
                nivelIngles = cbxIngles.Text,
                idPuesto = (Puesto)cbxPuesto.SelectedItem
            };
            Console.Write(colaborador.id);
            cbo.Registrar(colaborador);
            Close();

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
