create database tarea3;

create table departamento(
	id serial primary key,
	nombre text not null
);
alter table departamento add constraint unq_nombre unique(nombre)

create table puesto(
	id serial primary key,
	nombre text not null,
	abreviacion text not null
);
alter table puesto add constraint unq_nom unique(nombre);
alter table puesto add constraint unq_abrev unique(abreviacion)

create table colaborador(
	id serial primary key,
	cedula text not null,
	nombre text not null,
	genero char default 'M',
	id_departamento int not null,
	fecha_nacimiento date not null,
	edad int not null,
	fecha_ingreso date not null,
	nivel_ingles text not null,
	id_puesto int not null,
	constraint fk_id_dep foreign key(id_departamento) references public.departamento(id), 
    constraint fk_id_puesto foreign key(id_puesto) references public.puesto(id),
	constraint unq_ced unique(cedula)
);

INSERT INTO public.departamento(nombre) VALUES ('Producción');
INSERT INTO public.departamento(nombre) VALUES ('Aseguramiento de la Calidad');
INSERT INTO public.departamento(nombre) VALUES ('Data Entry');



INSERT INTO public.puesto(nombre, abreviacion)VALUES ('Ingeniero de Software I', 'ISI'),
('Ingeniero de Software II' ,'ISII'),
('Senior' ,'S'),
('Ingeniero de Calidad','IC'),
('Arquitecto' ,'A')

select * from puesto;
select * from departamento;
select * from colaborador;

drop table colaborador

insert into colaborador(cedula, nombre, genero,
id_departamento, fecha_nacimiento, edad, fecha_ingreso,
nivel_ingles, id_puesto)VALUES ('207810510', 'Oky Madrigal Castro', 'F', 1, '07/07/1998', 21 ,'01/01/2019','B2', 2);

Select co.id, cedula, co.nombre, genero, b.nombre, 
fecha_nacimiento, edad, fecha_ingreso, nivel_ingles, p.abreviacion from  colaborador co
inner join departamento b on co.id_departamento = b.id
inner join puesto p on co.id_puesto = p.id 

Select co.id, cedula, co.nombre, genero, b.nombre, fecha_nacimiento, edad, 
fecha_ingreso, nivel_ingles, p.abreviacion from colaborador co inner join departamento b on 
co.id_departamento = b.id inner join puesto p on co.id_puesto = p.id 
where lower(cedula) like lower('207810510') or lower(b.nombre) like lower('Producción')

select d.id, d.nombre FROM public.departamento d
right join colaborador c on c.id_departamento=d.id
where c.id = 1

SELECT p.id, p.nombre, abreviacion FROM public.puesto p right join colaborador c on c.id_puesto = p.id WHERE c.id = 1

Select co.id, cedula, co.nombre, genero, b.nombre, fecha_nacimiento, edad, 
fecha_ingreso, nivel_ingles, p.abreviacion from colaborador co inner join departamento b on 
co.id_departamento = b.id inner join puesto p on co.id_puesto = p.id 
where lower(genero) like lower('F%') and lower(nivel_ingles) like lower('C1') or
fecha_ingreso BETWEEN CAST ('2018-12-01' AS DATE) AND CAST ('2019-02-02' AS DATE)

select count(edad) from colaborador where edad between 20 and 21