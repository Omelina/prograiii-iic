﻿
using Entities;
using System;
using System.Collections.Generic;


namespace GUI
{
    class Menu
    {
        static int LeerInt(string msj)
        {
            Console.Write(msj + " ");
            int op = Int32.Parse(Console.ReadLine());
            return op;
        }

        static void Main(string[] args)
        {
            string menu1 = "Sistema de Choferes y Camiones: \n" +
                "1. Agregar Datos\n" +
                "2. Consultar\n" +
                "3. Salir";
            string menu2 = "Tipo de Camión: \n" +
                "1. Carga pesada \n" +
                "2. Carga ancha \n" +
                "3. Carga liviana";

            string menu3 = "Tipo de Carga: \n" +
                "1. Carga peligrosa\n" +
                "2. Carga no peligrosa";


            Chofer chofer = new Chofer();
            Camion camion = new Camion();
            List<Chofer> lista;
            lista = new List<Chofer>();

            while (true)
            {
                Console.WriteLine(menu1);
                Console.Write("Seleccione una opción: ");
                int op1 = Int32.Parse(Console.ReadLine());
                Console.Clear();
                if (op1 == 1)
                {
                    Console.WriteLine("Escriba la cedula del chofer: ");
                    string cedula = Console.ReadLine();
                    chofer.cedula = cedula;
                    Console.WriteLine("Escriba el nombre del chofer: ");
                    string nombre = Console.ReadLine();
                    chofer.nombre = nombre;
                    Console.WriteLine("Escriba la edad del chofer: ");
                    string edad = Console.ReadLine();
                    chofer.nombre = edad;
                    Console.WriteLine(menu2);
                    Console.Write("Seleccione una opción: ");
                    int op2 = Int32.Parse(Console.ReadLine());
                    Console.Clear();
                    if (op2 == 1)
                    {
                        camion.tipoCamion = "Carga pesada";
                    }
                    else if (op2 == 2)
                    {
                        camion.tipoCamion = "Carga ancha";
                    }
                    else
                    {
                        camion.tipoCamion = "Carga liviana";
                    }
                    Console.WriteLine(menu3);
                    Console.Write("Seleccione una opción: ");
                    int op3 = Int32.Parse(Console.ReadLine());
                    Console.Clear();
                    if (op3 == 1)
                    {
                        camion.tipoCarga = "Carga peligrosa";
                    }
                    else
                    {
                        camion.tipoCarga = "Carga no peligrosa";
                    }
                    chofer.camion = camion;
                    lista.Add(chofer);
                    Console.WriteLine("Chofer y camión registrado, presione cualquier tecla para salir: ");
                    Console.ReadKey();
                    Console.Clear();

                }
                else if (op1 == 2)
                {
                    Console.WriteLine("Escriba la cedula del chofer: ");
                    string cedula = Console.ReadLine();
                    foreach (var item in lista)
                    {
                        if (item.cedula == cedula)
                        {
                            Console.WriteLine(item.toString() + "\n" +
                                "Presione cualquier tecla para salir");
                            Console.ReadKey();
                            Console.Clear();
                            break;
                        }
                    }
                }
                else if (op1 == 3)
                {
                    goto SALIDA;
                }
            }
        SALIDA:
            Console.WriteLine("Gracias por utilizar la aplicación!!");
            Console.ReadKey();
        }
    }
}
