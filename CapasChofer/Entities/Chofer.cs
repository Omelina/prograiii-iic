﻿using System;

namespace Entities
{
    public class Chofer
    {
        public string nombre { get; set; }
        public string cedula { get; set; }
        public int edad { get; set; }

        public Chofer(string nombre, string cedula, int edad)
        {
            this.nombre = nombre;
            this.cedula = cedula;
            this.edad = edad;
        }

        public Chofer()
        {
        }

        public Camion camion { get; set; }



        public String toString()
        {
            return cedula + ", " + nombre + ", " + edad + ","+ camion.tipoCamion + ", " + camion.tipoCarga;
        }
    }
}
