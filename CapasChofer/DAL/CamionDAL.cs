﻿using System;
using System.Collections.Generic;
using System.Text;
using Npgsql;

namespace DAL
{
    class CamionDAL
    {
        static NpgsqlConnection con;
        static NpgsqlCommand cmd;


        public void InsertarCamion(string tipoCamion, string tipoCarga)
        {
            ClsConexion.Conexion();
            con.Open();
            cmd = new NpgsqlCommand("INSERT INTO camion (tipo_camion, tipo_carga) VALUES ('" + tipoCamion + "', '" + tipoCarga + "')", con);
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public void ModificarChofer(int id,int tipoCamion, string tipoCarga)
        {
            ClsConexion.Conexion();
            con.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("UPDATE camion SET tipo_camion = '" + tipoCamion + "', tipo_carga='" + tipoCarga + "' WHERE id = '" +id + "'", con);
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public void EliminarChofer(int id)
        {
            ClsConexion.Conexion();
            con.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("DELETE FROM camion WHERE id = '" + id + "'", con);
            cmd.ExecuteNonQuery();
            con.Close();
        }
    }
}
