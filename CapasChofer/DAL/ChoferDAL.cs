﻿using System;
using System.Collections.Generic;
using System.Text;
using Npgsql;

namespace DAL
{
    class ChoferDAL
    {
        static NpgsqlConnection con;
        static NpgsqlCommand cmd; 


        public void InsertarChofer(int cedula, string nombre, int edad)
        {
            ClsConexion.Conexion();
            con.Open();
            cmd = new NpgsqlCommand("INSERT INTO chofer (cedula, nombre, edad) VALUES ('" + cedula + "', '" + nombre + "', '" + edad + "')", con);
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public void ModificarChofer(int cedula, string nombre, int edad)
        {
            ClsConexion.Conexion();
            con.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("UPDATE chofer SET nombre = '" + nombre + "', edad = '" + edad + "' WHERE cedula = '" + cedula + "'", con);
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public void EliminarChofer(int cedula)
        {
            ClsConexion.Conexion();
            con.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("DELETE FROM chofer WHERE cedula = '" + cedula + "'", con);
            cmd.ExecuteNonQuery();
            con.Close();
        }
    }
}
