﻿using System;
using System.Dynamic;

namespace TareaI
{
    class Chofer
    {
        public string nombre { get; set; }
        public string cedula { get; set; }

        public Chofer(string cedula, string nombre)
        {
            this.cedula = cedula;
            this.nombre = nombre;
        }

        public Chofer()
        {
        }


        public Camion camion { get; set; }

        

        public String toString()
        {
            return cedula + ", " + nombre +", "+ camion.tipoCamion+ ", "+camion.tipoCarga;
        }
    }
}
