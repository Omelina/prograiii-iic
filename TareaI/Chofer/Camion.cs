﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TareaI
{
    class Camion
    {
        public string tipoCamion { get; set; }
        public string tipoCarga { get; set; }

        public Camion(string tipoCamion, string tipoCarga)
        {
            this.tipoCamion = tipoCamion;
            this.tipoCarga = tipoCarga;
        }

        public Camion()
        {
        }

        public String toString()
        {
            return tipoCamion + "," + tipoCarga;
        }
    }
}
