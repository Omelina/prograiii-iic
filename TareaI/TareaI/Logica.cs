﻿using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Text;

namespace TareaI
{
    class Logica { 

        internal string Rimas(string palabra1, string palabra2)
        {
            int longi1 = palabra1.Length;
            int longi2 = palabra2.Length;
            if ((palabra1.Substring(longi1-3, 3)) == (palabra2.Substring(longi2-3,3)))
            {
                return "Las palabras riman";
            }else if (palabra1.Substring(longi1-2, 2) == palabra2.Substring(longi2-2, 2))
            {
                return "Las palabras riman un poco";
            }
            else
            {
                return "Las palabras no riman";
            }
        }

        internal int NumeroSuerte(int num1, int num2, int num3)
        {
            int suma = num1 + num2 + num3;
            int s=0;
            Console.WriteLine(suma);
            String x = suma.ToString();
            char[] arreglo = x.ToCharArray();
            for(int i= 0; i < arreglo.Length;i++)
            {
                int n = int.Parse(arreglo[i].ToString());
                s += n;

            }
            return s;
        }

        internal void NumeroPerfecto(int num1)
        {
            int sum = 0;

            for (int i = 1; i < num1; i++)
            {
                if (num1 % i == 0)
                {
                    sum = sum + i;
                }

            }

            if (sum == num1) { Console.WriteLine("El numero" +num1+ "es perfecto"); }
            else { Console.WriteLine("El numero "+num1+" no es perfecto"); }

        }

        internal void AdivinaNum()
        {
            int opor = 3;
            int adi;
            int num = 0;
            Random ran = new Random();
            do
            {
                adi = Convert.ToInt32(ran.Next(20));
            } while (!(adi >= 1) & (adi <= 20));
            Console.WriteLine(adi);
            while (opor != 0)
            {
                Console.WriteLine("Ingresa un número del 1 al 20: ");
                string linea = Console.ReadLine();
                num = int.Parse(linea);
                if (adi == num)
                {
                    Console.WriteLine("ADIVINASTE!!");
                    break;
                }
                if (adi < num)
                {
                    Console.WriteLine("Te pasaste, tienes otra oportunidad");
                    opor--;
                }
                else if (adi > num)
                {
                    Console.WriteLine("Estás más abajo, tienes otra oportunidad");
                    opor--;
                }
            }
            if (opor == 0)
            {
                Console.Write("PERDISTE \n" +
                        "El numero era: " + adi);
            }

        }
    }

}
