﻿using System;

namespace TareaI
{
    class Principal
    {
        static int LeerInt(string msj)
        {
            Console.Write(msj + " ");
            int op = Int32.Parse(Console.ReadLine());
            return op;
        }

        static void Main(string[] args)
        {
            string menu = "Tarea 1 - UTN\n" +
                "1. Numero de la suerte\n" +
                "2. Numero perfecto\n" +
                "3. Adivina el número\n" +
                "4. Buscar las rimas\n" +
                "5. Salir";

            Logica ope = new Logica();

            while (true)
            {
                Console.WriteLine(menu);
                Console.Write("Seleccione una opción: ");
                int op = Int32.Parse(Console.ReadLine());
                Console.Clear();
                if (op ==1)
                {
                    int n1 = LeerInt("Digite su día de nacimiento");
                    int n2 = LeerInt("Mes de nacimiento");
                    int n3 = LeerInt("Año de nacimiento");
                    int result = ope.NumeroSuerte(n1, n2, n3);
                    Console.WriteLine(result + "Es su número de la suerte");
                }
                else if (op == 2)
                {
                    int n1 = LeerInt("Digite el numero que desea saber si es perfecto: ");
                    ope.NumeroPerfecto(n1);
                }
                else if (op == 3)
                {
                    ope.AdivinaNum();
                }
                else if (op == 4)
                {
                    Console.WriteLine("Escriba la primera palabra: ");
                    string palabra1 = Console.ReadLine();
                    Console.WriteLine("Escriba la segunda palabra: ");
                    string palabra2 = Console.ReadLine();
                    //ope.Rimas(palabra1,palabra2);
                    Console.WriteLine(ope.Rimas(palabra1, palabra2));
                }
                else if (op == 5)
                {
                    goto SALIDA;
                }
            }
        SALIDA:
            Console.WriteLine("Gracias por utilizar la aplicación!!");
            Console.ReadKey();
        }

    }
}
