﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerInterfaz
{
    public partial class MenúPractica : Form
    {
        public MenúPractica()
        {
            InitializeComponent();
        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            int numero;

            for (numero=1; numero <=100; numero++)
            {
                if (numero % 3 == 0)
                {
                    listBox1.Items.Add(numero);
                }
            }
            
        }

        private void btnInicio_Click(object sender, EventArgs e)
        {
            listaPares.Items.Clear();
            listaImpares.Items.Clear();
            var random = new Random();
            for (int i = 0; i <=20; i++)
            {
                int num = random.Next(1, 100);

                if (num % 2 == 0)
                {
                    listaPares.Items.Add(num);
                }
                else if (num % 3 == 0)
                {
                    listaImpares.Items.Add(num);
                }
            }
                
        }

        private void btnPrimo_Click(object sender, EventArgs e)
        { 
            int a = 0, i, n;
            n = int.Parse(txtNumero.Text.Trim());
            for (i = 1; i < (n + 1); i++)
            {
                if (n % i == 0)
                {
                    a++;
                }
            }
            if (a != 2)
            {
                MessageBox.Show("El número "+n+" no es primo");
            }
            else
            {
                MessageBox.Show("El número "+n+" si es primo");
            }
        }

        private void btnConvertir_Click(object sender, EventArgs e)
        {
            if (txtCent.Text!="0.00")
            {
                int c = int.Parse(txtCent.Text.Trim());
                double f = (c * 9 / 5) + (32);
                txtFar.Clear();
                txtFar.Text = f.ToString();
            }
            else
            {
                int f = int.Parse(txtFar.Text.Trim());
                double c = (f * 9 / 5) + (32);
                txtCent.Clear();
                txtCent.Text = c.ToString();
            }
        }

        private void txtCent_Click(object sender, EventArgs e)
        {
            txtCent.Clear();
            txtFar.Clear();
            txtFar.Text = "0.00";
        }

        private void txtFar_Click(object sender, EventArgs e)
        {
            txtFar.Clear();
            txtCent.Clear();
            txtCent.Text = "0.00";
        }

        private void btnAparecer_Click(object sender, EventArgs e)
        {
            txtCambios.Visible = true;
        }

        private void btnDesaparecer_Click(object sender, EventArgs e)
        {
            txtCambios.Visible = false;
        }

        private void btnBloquear_Click(object sender, EventArgs e)
        {
            txtCambios.Enabled = false;
        }

        private void btnDesbloquear_Click(object sender, EventArgs e)
        {
            txtCambios.Enabled = true;
        }

        private void btnLetra_Click(object sender, EventArgs e)
        {
            if (cbxColores.SelectedIndex==0)
            {
                txtCambios.ForeColor = Color.Blue;
            }else if (cbxColores.SelectedIndex == 1)
            {
                txtCambios.ForeColor = Color.Orange;
            }else if (cbxColores.SelectedIndex==2)
            {
                txtCambios.ForeColor = Color.Yellow;
            }else if (cbxColores.SelectedIndex == 3)
            {
                txtCambios.ForeColor = Color.Red;
            }else if(cbxColores.SelectedIndex==4)
            {
                txtCambios.ForeColor = Color.Green;
            }else
            {
                txtCambios.ForeColor = Color.Black;
            }
        }

        private void btnFondo_Click(object sender, EventArgs e)
        {
            if (cbxColores.SelectedIndex == 0)
            {
                txtCambios.BackColor = Color.Blue;
            }
            else if (cbxColores.SelectedIndex == 1)
            {
                txtCambios.BackColor = Color.Orange;
            }
            else if (cbxColores.SelectedIndex == 2)
            {
                txtCambios.BackColor = Color.Yellow;
            }
            else if (cbxColores.SelectedIndex == 3)
            {
                txtCambios.BackColor = Color.Red;
            }
            else if (cbxColores.SelectedIndex == 4)
            {
                txtCambios.BackColor = Color.Green;
            }
            else
            {
                txtCambios.BackColor = Color.White;
            }
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            string txt = txtTextoxd.Text.Trim();
            txtCambios.Text = txt;
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtCambios.Clear();
        }
        public static string ARomanos(int number)
        {
            if ((number < 0) || (number > 3999)) 
                throw new ArgumentOutOfRangeException("Inserte un numero entre 1 y 3999");
            if (number < 1) return string.Empty;
            if (number >= 1000) return "M" + ARomanos(number - 1000);
            if (number >= 900) return "CM" + ARomanos(number - 900); 
            if (number >= 500) return "D" + ARomanos(number - 500);
            if (number >= 400) return "CD" + ARomanos(number - 400);
            if (number >= 100) return "C" + ARomanos(number - 100);
            if (number >= 90) return "XC" + ARomanos(number - 90);
            if (number >= 50) return "L" + ARomanos(number - 50);
            if (number >= 40) return "XL" + ARomanos(number - 40);
            if (number >= 10) return "X" + ARomanos(number - 10);
            if (number >= 9) return "IX" + ARomanos(number - 9);
            if (number >= 5) return "V" + ARomanos(number - 5);
            if (number >= 4) return "IV" + ARomanos(number - 4);
            if (number >= 1) return "I" + ARomanos(number - 1);
            throw new ArgumentOutOfRangeException("Algo salió mal");
        }

        private void btnRomanos_Click(object sender, EventArgs e)
        {
            int num = int.Parse(txtDecimal.Text.Trim());
            txtRomanos.Text = ARomanos(num);
        }
    }
    
}
