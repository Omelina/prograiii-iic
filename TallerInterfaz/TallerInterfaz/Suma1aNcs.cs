﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerInterfaz
{
    public partial class Suma1aNcs : Form
    {
        public Suma1aNcs()
        {
            InitializeComponent();
        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            int x, N, suma = 0;
            listBox1.Items.Clear();
            N = int.Parse(txtN.Text.Trim());
            for (x = 0; x <= N; x++)
            {
                suma = suma + x;
                if (checkBox1.Checked == true)
                {
                    listBox1.Items.Add("Sumando: " + x + " Suma Parcial : " + suma);
                }
            }
            listBox1.Items.Add("La suma total es: "+ suma);
        }
    }
}
