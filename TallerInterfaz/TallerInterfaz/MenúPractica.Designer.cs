﻿namespace TallerInterfaz
{
    partial class MenúPractica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.btnIniciar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnInicio = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.listaImpares = new System.Windows.Forms.ListBox();
            this.listaPares = new System.Windows.Forms.ListBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnPrimo = new System.Windows.Forms.Button();
            this.txtNumero = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.btnConvertir = new System.Windows.Forms.Button();
            this.txtFar = new System.Windows.Forms.TextBox();
            this.txtCent = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCambios = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btnAparecer = new System.Windows.Forms.Button();
            this.btnDesaparecer = new System.Windows.Forms.Button();
            this.btnBloquear = new System.Windows.Forms.Button();
            this.btnDesbloquear = new System.Windows.Forms.Button();
            this.btnLetra = new System.Windows.Forms.Button();
            this.btnFondo = new System.Windows.Forms.Button();
            this.btnEnviar = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.txtTextoxd = new System.Windows.Forms.TextBox();
            this.cbxColores = new System.Windows.Forms.ComboBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.label11 = new System.Windows.Forms.Label();
            this.txtDecimal = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtRomanos = new System.Windows.Forms.TextBox();
            this.btnRomanos = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Location = new System.Drawing.Point(40, 43);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(419, 308);
            this.listBox1.TabIndex = 0;
            // 
            // btnIniciar
            // 
            this.btnIniciar.Location = new System.Drawing.Point(345, 367);
            this.btnIniciar.Name = "btnIniciar";
            this.btnIniciar.Size = new System.Drawing.Size(114, 37);
            this.btnIniciar.TabIndex = 1;
            this.btnIniciar.Text = "Iniciar";
            this.btnIniciar.UseVisualStyleBackColor = true;
            this.btnIniciar.Click += new System.EventHandler(this.btnIniciar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(177, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Multiplos de 3 del 1 al 100:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Location = new System.Drawing.Point(2, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(510, 464);
            this.tabControl1.TabIndex = 3;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnIniciar);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.listBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(502, 435);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Multiplos";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnInicio);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.listaImpares);
            this.tabPage2.Controls.Add(this.listaPares);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(502, 435);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "20 Números";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnInicio
            // 
            this.btnInicio.Location = new System.Drawing.Point(175, 342);
            this.btnInicio.Name = "btnInicio";
            this.btnInicio.Size = new System.Drawing.Size(149, 38);
            this.btnInicio.TabIndex = 4;
            this.btnInicio.Text = "Iniciar";
            this.btnInicio.UseVisualStyleBackColor = true;
            this.btnInicio.Click += new System.EventHandler(this.btnInicio_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(285, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Números Impares:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Números Pares:";
            // 
            // listaImpares
            // 
            this.listaImpares.FormattingEnabled = true;
            this.listaImpares.ItemHeight = 16;
            this.listaImpares.Location = new System.Drawing.Point(288, 44);
            this.listaImpares.Name = "listaImpares";
            this.listaImpares.Size = new System.Drawing.Size(197, 244);
            this.listaImpares.TabIndex = 1;
            // 
            // listaPares
            // 
            this.listaPares.FormattingEnabled = true;
            this.listaPares.ItemHeight = 16;
            this.listaPares.Location = new System.Drawing.Point(20, 44);
            this.listaPares.Name = "listaPares";
            this.listaPares.Size = new System.Drawing.Size(197, 244);
            this.listaPares.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnPrimo);
            this.tabPage3.Controls.Add(this.txtNumero);
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(502, 435);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Números Primos";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btnPrimo
            // 
            this.btnPrimo.Location = new System.Drawing.Point(41, 129);
            this.btnPrimo.Name = "btnPrimo";
            this.btnPrimo.Size = new System.Drawing.Size(402, 43);
            this.btnPrimo.TabIndex = 2;
            this.btnPrimo.Text = "Calcular Número Primo";
            this.btnPrimo.UseVisualStyleBackColor = true;
            this.btnPrimo.Click += new System.EventHandler(this.btnPrimo_Click);
            // 
            // txtNumero
            // 
            this.txtNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumero.Location = new System.Drawing.Point(41, 73);
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.Size = new System.Drawing.Size(403, 30);
            this.txtNumero.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(38, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(131, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Ingrese un número:";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.btnConvertir);
            this.tabPage4.Controls.Add(this.txtFar);
            this.tabPage4.Controls.Add(this.txtCent);
            this.tabPage4.Controls.Add(this.label6);
            this.tabPage4.Controls.Add(this.label5);
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(502, 435);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Convertidor de C° Y F°";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // btnConvertir
            // 
            this.btnConvertir.Location = new System.Drawing.Point(45, 206);
            this.btnConvertir.Name = "btnConvertir";
            this.btnConvertir.Size = new System.Drawing.Size(381, 40);
            this.btnConvertir.TabIndex = 3;
            this.btnConvertir.Text = "Convertir";
            this.btnConvertir.UseVisualStyleBackColor = true;
            this.btnConvertir.Click += new System.EventHandler(this.btnConvertir_Click);
            // 
            // txtFar
            // 
            this.txtFar.Location = new System.Drawing.Point(45, 150);
            this.txtFar.Name = "txtFar";
            this.txtFar.Size = new System.Drawing.Size(381, 22);
            this.txtFar.TabIndex = 2;
            this.txtFar.Text = "0.00";
            this.txtFar.Click += new System.EventHandler(this.txtFar_Click);
            // 
            // txtCent
            // 
            this.txtCent.Location = new System.Drawing.Point(45, 85);
            this.txtCent.Name = "txtCent";
            this.txtCent.Size = new System.Drawing.Size(381, 22);
            this.txtCent.TabIndex = 1;
            this.txtCent.Text = "0,00";
            this.txtCent.Click += new System.EventHandler(this.txtCent_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(42, 120);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(174, 17);
            this.label6.TabIndex = 1;
            this.label6.Text = "Ingrese Grados Farenheit:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(42, 50);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(190, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "Ingrese Grados Centigrados:";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.cbxColores);
            this.tabPage5.Controls.Add(this.txtTextoxd);
            this.tabPage5.Controls.Add(this.btnLimpiar);
            this.tabPage5.Controls.Add(this.btnEnviar);
            this.tabPage5.Controls.Add(this.btnFondo);
            this.tabPage5.Controls.Add(this.btnLetra);
            this.tabPage5.Controls.Add(this.btnDesbloquear);
            this.tabPage5.Controls.Add(this.btnBloquear);
            this.tabPage5.Controls.Add(this.btnDesaparecer);
            this.tabPage5.Controls.Add(this.btnAparecer);
            this.tabPage5.Controls.Add(this.label12);
            this.tabPage5.Controls.Add(this.label10);
            this.tabPage5.Controls.Add(this.label9);
            this.tabPage5.Controls.Add(this.label8);
            this.tabPage5.Controls.Add(this.txtCambios);
            this.tabPage5.Controls.Add(this.label7);
            this.tabPage5.Location = new System.Drawing.Point(4, 25);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(502, 435);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Propiedades";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(90, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(300, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "MODIFICANDO PROPIEDADES DE CONTROL";
            // 
            // txtCambios
            // 
            this.txtCambios.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCambios.Location = new System.Drawing.Point(58, 45);
            this.txtCambios.Name = "txtCambios";
            this.txtCambios.Size = new System.Drawing.Size(381, 45);
            this.txtCambios.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(296, 114);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 17);
            this.label8.TabIndex = 2;
            this.label8.Text = "Accesibilidad:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(55, 250);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(168, 17);
            this.label9.TabIndex = 3;
            this.label9.Text = "Color de Fuente y Fondo:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(296, 248);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 17);
            this.label10.TabIndex = 4;
            this.label10.Text = "Texto:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(55, 114);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(75, 17);
            this.label12.TabIndex = 6;
            this.label12.Text = "Visibilidad:";
            // 
            // btnAparecer
            // 
            this.btnAparecer.Location = new System.Drawing.Point(58, 146);
            this.btnAparecer.Name = "btnAparecer";
            this.btnAparecer.Size = new System.Drawing.Size(154, 29);
            this.btnAparecer.TabIndex = 7;
            this.btnAparecer.Text = "Aparecerlo";
            this.btnAparecer.UseVisualStyleBackColor = true;
            this.btnAparecer.Click += new System.EventHandler(this.btnAparecer_Click);
            // 
            // btnDesaparecer
            // 
            this.btnDesaparecer.Location = new System.Drawing.Point(58, 190);
            this.btnDesaparecer.Name = "btnDesaparecer";
            this.btnDesaparecer.Size = new System.Drawing.Size(154, 29);
            this.btnDesaparecer.TabIndex = 8;
            this.btnDesaparecer.Text = "Desaparecerlo";
            this.btnDesaparecer.UseVisualStyleBackColor = true;
            this.btnDesaparecer.Click += new System.EventHandler(this.btnDesaparecer_Click);
            // 
            // btnBloquear
            // 
            this.btnBloquear.Location = new System.Drawing.Point(299, 146);
            this.btnBloquear.Name = "btnBloquear";
            this.btnBloquear.Size = new System.Drawing.Size(140, 29);
            this.btnBloquear.TabIndex = 9;
            this.btnBloquear.Text = "Bloquear Control";
            this.btnBloquear.UseVisualStyleBackColor = true;
            this.btnBloquear.Click += new System.EventHandler(this.btnBloquear_Click);
            // 
            // btnDesbloquear
            // 
            this.btnDesbloquear.Location = new System.Drawing.Point(299, 190);
            this.btnDesbloquear.Name = "btnDesbloquear";
            this.btnDesbloquear.Size = new System.Drawing.Size(140, 29);
            this.btnDesbloquear.TabIndex = 10;
            this.btnDesbloquear.Text = "Desbloquear";
            this.btnDesbloquear.UseVisualStyleBackColor = true;
            this.btnDesbloquear.Click += new System.EventHandler(this.btnDesbloquear_Click);
            // 
            // btnLetra
            // 
            this.btnLetra.Location = new System.Drawing.Point(58, 312);
            this.btnLetra.Name = "btnLetra";
            this.btnLetra.Size = new System.Drawing.Size(154, 46);
            this.btnLetra.TabIndex = 11;
            this.btnLetra.Text = "Cambiar el Color de Letra";
            this.btnLetra.UseVisualStyleBackColor = true;
            this.btnLetra.Click += new System.EventHandler(this.btnLetra_Click);
            // 
            // btnFondo
            // 
            this.btnFondo.Location = new System.Drawing.Point(58, 364);
            this.btnFondo.Name = "btnFondo";
            this.btnFondo.Size = new System.Drawing.Size(154, 47);
            this.btnFondo.TabIndex = 12;
            this.btnFondo.Text = "Cambiar el Color de Fondo";
            this.btnFondo.UseVisualStyleBackColor = true;
            this.btnFondo.Click += new System.EventHandler(this.btnFondo_Click);
            // 
            // btnEnviar
            // 
            this.btnEnviar.Location = new System.Drawing.Point(299, 312);
            this.btnEnviar.Name = "btnEnviar";
            this.btnEnviar.Size = new System.Drawing.Size(140, 46);
            this.btnEnviar.TabIndex = 13;
            this.btnEnviar.Text = "Enviarle Texto";
            this.btnEnviar.UseVisualStyleBackColor = true;
            this.btnEnviar.Click += new System.EventHandler(this.btnEnviar_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(299, 364);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(139, 47);
            this.btnLimpiar.TabIndex = 14;
            this.btnLimpiar.Text = "Limpiar Texto";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // txtTextoxd
            // 
            this.txtTextoxd.Location = new System.Drawing.Point(299, 272);
            this.txtTextoxd.Name = "txtTextoxd";
            this.txtTextoxd.Size = new System.Drawing.Size(139, 22);
            this.txtTextoxd.TabIndex = 15;
            // 
            // cbxColores
            // 
            this.cbxColores.FormattingEnabled = true;
            this.cbxColores.Items.AddRange(new object[] {
            "Azul",
            "Naranja",
            "Amarillo",
            "Rojo",
            "Verde"});
            this.cbxColores.Location = new System.Drawing.Point(58, 272);
            this.cbxColores.Name = "cbxColores";
            this.cbxColores.Size = new System.Drawing.Size(154, 24);
            this.cbxColores.TabIndex = 16;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.btnRomanos);
            this.tabPage6.Controls.Add(this.txtRomanos);
            this.tabPage6.Controls.Add(this.label13);
            this.tabPage6.Controls.Add(this.txtDecimal);
            this.tabPage6.Controls.Add(this.label11);
            this.tabPage6.Location = new System.Drawing.Point(4, 25);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(502, 435);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Números Romanos";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(38, 30);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(252, 17);
            this.label11.TabIndex = 0;
            this.label11.Text = "Escriba el número en notación decimal";
            // 
            // txtDecimal
            // 
            this.txtDecimal.Location = new System.Drawing.Point(40, 68);
            this.txtDecimal.Name = "txtDecimal";
            this.txtDecimal.Size = new System.Drawing.Size(293, 22);
            this.txtDecimal.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(38, 124);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(224, 17);
            this.label13.TabIndex = 2;
            this.label13.Text = "Equivalente en números romanos:";
            // 
            // txtRomanos
            // 
            this.txtRomanos.Enabled = false;
            this.txtRomanos.Location = new System.Drawing.Point(40, 165);
            this.txtRomanos.Name = "txtRomanos";
            this.txtRomanos.Size = new System.Drawing.Size(293, 22);
            this.txtRomanos.TabIndex = 3;
            // 
            // btnRomanos
            // 
            this.btnRomanos.Location = new System.Drawing.Point(40, 227);
            this.btnRomanos.Name = "btnRomanos";
            this.btnRomanos.Size = new System.Drawing.Size(153, 37);
            this.btnRomanos.TabIndex = 4;
            this.btnRomanos.Text = "Convertir";
            this.btnRomanos.UseVisualStyleBackColor = true;
            this.btnRomanos.Click += new System.EventHandler(this.btnRomanos_Click);
            // 
            // MenúPractica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(514, 466);
            this.Controls.Add(this.tabControl1);
            this.Name = "MenúPractica";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menú Práctica";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button btnIniciar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnInicio;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox listaImpares;
        private System.Windows.Forms.ListBox listaPares;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnPrimo;
        private System.Windows.Forms.TextBox txtNumero;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button btnConvertir;
        private System.Windows.Forms.TextBox txtFar;
        private System.Windows.Forms.TextBox txtCent;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Button btnEnviar;
        private System.Windows.Forms.Button btnFondo;
        private System.Windows.Forms.Button btnLetra;
        private System.Windows.Forms.Button btnDesbloquear;
        private System.Windows.Forms.Button btnBloquear;
        private System.Windows.Forms.Button btnDesaparecer;
        private System.Windows.Forms.Button btnAparecer;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtCambios;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbxColores;
        private System.Windows.Forms.TextBox txtTextoxd;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Button btnRomanos;
        private System.Windows.Forms.TextBox txtRomanos;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtDecimal;
        private System.Windows.Forms.Label label11;
    }
}