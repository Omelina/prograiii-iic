﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerInterfaz
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            string login, password;
            login = txtLogin.Text.TrimEnd();
            password = txtContra.Text.TrimEnd();
            if (login == "UAT" && password == "informatica")
            {
                MessageBox.Show("Bienvenido al sistema!");
            }
            else
            {
                MessageBox.Show("Acceso DENEGADO");
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
